import React, { useEffect } from "react";
import { observer } from "mobx-react";
import { useHistory } from "react-router-dom";
import { Title } from "../components/_title";
import { NewTable } from "../components/NewTable";
import { routesList } from "../assets/enums/routesList";
import { deleteKey, getValueFromObject } from "../assets/functions";

export const FieldsTemplates = observer(({ store }) => {
  const { fieldsTemplatesStore } = store;

  const initialData = fieldsTemplatesStore.fieldsTemplates;

  const history = useHistory();

  useEffect(() => {
    fieldsTemplatesStore.handleGetFieldsTemplateList();
  }, []);

  const tableHeadData = [
    {
      field: "name",
      title: "Name",
    },
    {
      field: "description",
      title: "Description",
    },
  ];

  const actions = [
    {
      icon: "subject",
      tooltip: "Show this template's fields",
      onClick: (event, rowData) => {
        const itemPath = `${routesList.FIELDS_TEMPLATES}/${rowData.id}`;
        history.push(itemPath);
      },
    },
  ];

  const onRowUpdate = (newData, oldData, dataUpdate, resolve, reject) => {
    fieldsTemplatesStore.setFieldsTemplates([...dataUpdate]);

    const clearNewData = deleteKey(newData, "fields");
    const clearOldData = deleteKey(oldData, "fields");

    fieldsTemplatesStore.handlePostFildsTemplateUpdate(
      getValueFromObject(clearNewData, clearOldData, "id"),
      initialData,
      resolve,
      reject
    );
  };

  const onRowAdd = (newData, dataAdd, resolve, reject) => {
    fieldsTemplatesStore.setFieldsTemplates(dataAdd);
    fieldsTemplatesStore.handlePostFildsTemplateAdd(
      newData,
      initialData,
      resolve,
      reject
    );
  };

  const onRowDelete = (oldData, dataDelete, resolve, reject) => {
    fieldsTemplatesStore.setFieldsTemplates([...dataDelete]);

    const newData = { id: oldData.id };

    fieldsTemplatesStore.handlePostFildsTemplateRemove(
      newData,
      initialData,
      resolve,
      reject
    );
  };

  return (
    <>
      <Title label="Fields templates" blueGradient />
      <NewTable
        title="Fields templates"
        columns={tableHeadData}
        data={initialData}
        disableFiltering
        actions={actions}
        onRowUpdate={onRowUpdate}
        onRowAdd={onRowAdd}
        onRowDelete={onRowDelete}
      />
    </>
  );
});
