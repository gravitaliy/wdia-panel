import React, { useEffect } from "react";
import { useRouteMatch } from "react-router-dom";
import { observer } from "mobx-react";
import { NewTable } from "../components/NewTable";
import { routesList } from "../assets/enums/routesList";
import { TitleWithBack } from "../components/_title";
import { getLookup } from "../assets/functions";

export const UsersCompany = observer(({ store }) => {
  const { usersCompanyStore, commonResourcesStore, rolesStore } = store;

  const initialData = usersCompanyStore.users;

  const match = useRouteMatch();

  useEffect(() => {
    usersCompanyStore.handleGetCompanyUsers(match.params.id);
    usersCompanyStore.handleGetCompanyById(match.params.id);
    commonResourcesStore.handleGetCommonResources();
    rolesStore.handleGetRoles();
  }, []);

  const lookupRole = Object.assign({},
    ...rolesStore.roles.map(({ name }) => ({
      [name]: name
    })));

  const tableHeadData = [
    {
      field: "firstname",
      title: "First name",
      filtering: false,
      editable: "never",
    },
    {
      field: "lastname",
      title: "Last name",
      filtering: false,
      editable: "never",
    },
    {
      field: "email",
      title: "E-mail",
      filtering: false,
      editable: "never",
    },
    {
      field: "state",
      title: "State",
      lookup: getLookup(commonResourcesStore?.commonResources?.states?.USER),
    },
    {
      field: "role",
      title: "Role",
      lookup: lookupRole,
    },
  ];

  const onCellEditApproved = (
    newValue,
    rowData,
    columnDef,
    dataUpdate,
    resolve,
    reject
  ) => {
    usersCompanyStore.setCompanyUsers([...dataUpdate]);

    const newData = {
      userId: rowData.id,
      [columnDef.field]: newValue,
    };

    if (columnDef.field === "role") {
      usersCompanyStore.handlePostUserChangeRole(
        newData,
        initialData,
        resolve,
        reject
      );
    }
    if (columnDef.field === "state") {
      usersCompanyStore.handlePostUserChangeState(
        newData,
        initialData,
        resolve,
        reject
      );
    }
  };

  return (
    <>
      <TitleWithBack
        backLink={routesList.COMPANIES}
        label={`Users company - ${usersCompanyStore.company.name || ""}`}
      />
      <NewTable
        title="Users Company"
        columns={tableHeadData}
        data={initialData}
        cellEditable={onCellEditApproved}
      />
    </>
  );
});
