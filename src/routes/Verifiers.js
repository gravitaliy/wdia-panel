import React, { useEffect, useState } from "react";
import { observer } from "mobx-react";
import { useHistory } from "react-router-dom";
import { Title } from "../components/_title";
import { NewTable } from "../components/NewTable";
import { routesList } from "../assets/enums/routesList";
import { AddModal } from "../components/_popups";
import {
  getLookup,
  getValueFromObject,
  renderDate,
} from "../assets/functions";

export const Verifiers = observer(({ store }) => {
  const { verifiersStore, servicesStore, commonResourcesStore } = store;

  const initialData = servicesStore.services;

  const history = useHistory();

  const [open, setOpen] = useState(false);
  const [selectedVerifier, setselectedVerifier] = useState({});

  useEffect(() => {
    verifiersStore.handleGetVerifierList();
    servicesStore.handleGetServiceList();
    commonResourcesStore.handleGetCommonResources();
  }, []);

  const tableHeadDataGeneral = [
    {
      field: "name",
      title: "Name",
      filtering: false,
    },
    {
      field: "contactEmail",
      title: "Contact e-mail",
      filtering: false,
    },
    {
      field: "contactPhone",
      title: "Contact phone",
      filtering: false,
    },
    {
      field: "description",
      title: "Description",
      filtering: false,
    },
    {
      field: "url",
      title: "URL",
      filtering: false,
    },
    {
      field: "backendUrl",
      title: "Backend URL",
      filtering: false,
    },
    {
      field: "createdAt",
      title: "Created at",
      filtering: false,
      editable: "never",
      render: rowData => renderDate(rowData.createdAt),
    },
    {
      field: "state",
      title: "State",
      lookup: getLookup(commonResourcesStore?.commonResources?.states?.VERIFIER),
    },
  ];

  const tableHeadDataModal = [
    {
      field: "name",
      title: "Name",
      filtering: false,
    },
    {
      field: "description",
      title: "Description",
      filtering: false,
    },
    {
      field: "url",
      title: "URL",
      filtering: false,
    },
    {
      field: "type",
      title: "Type",
      editable: "never",
      lookup: getLookup(commonResourcesStore?.commonResources?.types?.SERVICE),
    },
  ];

  const actions = [
    {
      icon: "storage",
      tooltip: "Show verifier's services",
      onClick: (event, rowData) => {
        const itemPath = `${routesList.VERIFIERS}/${rowData.hash}`;
        history.push(itemPath);
      },
    },
    {
      icon: "library_add",
      tooltip: "Add services to verifier",
      onClick: (event, rowData) => {
        setselectedVerifier(rowData);
        handleToggle();
      },
    },
  ];

  const handleToggle = () => {
    setOpen(!open);
  };

  const onRowUpdate = (newData, oldData, dataUpdate, resolve, reject) => {
    verifiersStore.setVerifiers([...dataUpdate]);

    verifiersStore.handlePostUpdateVerifier(
      getValueFromObject(newData, oldData, "hash"),
      initialData,
      resolve,
      reject
    );
  };

  const addHandle = data => {
    verifiersStore.handlePostAddServicesToVerifier(data);
  };

  return (
    <>
      <Title label="Verifiers" blueGradient />
      <NewTable
        title="Verifiers"
        columns={tableHeadDataGeneral}
        data={verifiersStore.verifiers}
        onRowUpdate={onRowUpdate}
        actions={actions}
      />
      <AddModal
        open={open}
        handleToggle={handleToggle}
        title="Services"
        columns={tableHeadDataModal}
        data={initialData}
        addHandle={addHandle}
        selectedRow={selectedVerifier}
        actionTooltip="Add verifier to services"
        addType="verifier"
      />
    </>
  );
});
