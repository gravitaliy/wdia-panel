import React, { useState } from "react";
import { Box, makeStyles } from "@material-ui/core";
import { observer } from "mobx-react";
import { Title } from "../components/_title";
import { CardWrap, CardsRow } from "../components/_cards";
import { MyInputLabel } from "../components/MyInputLabel";
import { CustomButton } from "../components/CustomButton";

const useStyles = makeStyles(() => ({
  container: {
    boxSizing: "content-box",
    maxWidth: 540,
    margin: "0 auto",
    padding: "100px 15px 120px",
  },
  buttonsWrap: {
    marginTop: 60,
  },
  lastBtnWrap: {
    display: "block",
    textDecoration: "none",
    marginTop: 15,
  },
}));

export const SignIn = observer(({ store }) => {
  const classes = useStyles();

  const [isPending, setIsPending] = useState(false);

  const setSignIn = (name, value) => {
    store.setSignIn(name, value);
  };

  const handleSubmitForm = e => {
    e.preventDefault();
    store.handlePostAdminLogin(setIsPending);
  };

  return (
    <Box className={classes.container}>
      <Title label="Authorization" blueGradient />

      <CardsRow>
        <CardWrap size="grow">
          <form onSubmit={handleSubmitForm}>
            <Box>
              <MyInputLabel
                required
                label="E-MAIL"
                placeholder="Enter your e-mail"
                type="email"
                name="email"
                value={store.signIn.email}
                setItem={setSignIn}
              />
              <MyInputLabel
                required
                label="PASSWORD"
                placeholder="Enter your password"
                type="password"
                name="password"
                value={store.signIn.password}
                setItem={setSignIn}
              />
            </Box>
            <Box className={classes.buttonsWrap}>
              <CustomButton
                color="darkBlue"
                size="xlg-max"
                submit
                disabledIsPending={isPending}
              >
                Login
              </CustomButton>
            </Box>
          </form>
        </CardWrap>
      </CardsRow>
    </Box>
  );
});
