import React, { useEffect } from "react";
import { observer } from "mobx-react";
import { Title } from "../components/_title";
import { NewTable } from "../components/NewTable";
import { getLookup, renderDate } from "../assets/functions";

export const Questions = observer(({ store }) => {
  const { questionsStore, commonResourcesStore } = store;

  const initialData = questionsStore.questions;

  useEffect(() => {
    questionsStore.handleGetQuestionsList();
    commonResourcesStore.handleGetCommonResources();
  }, []);

  const tableHeadData = [
    {
      field: "createdAt",
      title: "Created at",
      filtering: false,
      editable: "never",
      render: rowData => renderDate(rowData.createdAt),
    },
    {
      field: "email",
      title: "E-mail",
      filtering: false,
      editable: "never",
    },
    {
      field: "firstname",
      title: "First name",
      filtering: false,
      editable: "never",
    },
    {
      field: "lastname",
      title: "Last name",
      filtering: false,
      editable: "never",
    },
    {
      field: "subject",
      title: "subject at",
      filtering: false,
      editable: "never",
    },
    {
      field: "source",
      title: "source",
      editable: "never",
      lookup: getLookup(commonResourcesStore?.commonResources?.sources?.QUESTION),
    },
    {
      field: "state",
      title: "State",
      lookup: getLookup(commonResourcesStore?.commonResources?.states?.QUESTION),
    },
  ];

  const onCellEditApproved = (
    newValue,
    rowData,
    columnDef,
    dataUpdate,
    resolve,
    reject
  ) => {
    questionsStore.setQuestions([...dataUpdate]);

    const newData = {
      id: rowData.id,
      [columnDef.field]: newValue,
    };
    questionsStore.handlePostQuestionChangeState(
      newData,
      initialData,
      resolve,
      reject
    );
  };

  return (
    <>
      <Title label="Questions" blueGradient />
      <NewTable
        title="Questions"
        columns={tableHeadData}
        data={initialData}
        cellEditable={onCellEditApproved}
      />
    </>
  );
});
