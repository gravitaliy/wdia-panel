import React, { useEffect } from "react";
import { observer } from "mobx-react";
import { Title } from "../components/_title";
import { NewTable } from "../components/NewTable";
import { getLookup, getValueFromObject } from "../assets/functions";

export const Roles = observer(({ store }) => {
  const { rolesStore } = store;

  const initialData = rolesStore.roles;

  useEffect(() => {
    rolesStore.handleGetRoles();
    rolesStore.handleGetModules();
  }, []);

  const tableHeadData = [
    {
      field: "name",
      title: "Name",
    },
    {
      field: "module",
      title: "Module",
      lookup: getLookup(rolesStore.modules),
    },
    {
      field: "permissions",
      title: "Permissions",
    },
  ];

  const onRowUpdate = (newData, oldData, dataUpdate, resolve, reject) => {
    rolesStore.setRoles([...dataUpdate]);

    rolesStore.handlePostRoleUpdate(
      getValueFromObject(newData, oldData, "id"),
      initialData,
      resolve,
      reject
    );
  };

  const onRowAdd = (newData, dataAdd, resolve, reject) => {
    rolesStore.setRoles(dataAdd);
    rolesStore.handlePostRoleAdd(
      newData,
      initialData,
      resolve,
      reject
    );
  };

  const onRowDelete = (oldData, dataDelete, resolve, reject) => {
    rolesStore.setRoles([...dataDelete]);

    const newData = { id: oldData.id };

    rolesStore.handlePostRoleRemove(
      newData,
      initialData,
      resolve,
      reject
    );
  };

  return (
    <>
      <Title label="Roles" blueGradient />
      <NewTable
        title="Roles"
        columns={tableHeadData}
        data={initialData}
        disableFiltering
        onRowUpdate={onRowUpdate}
        onRowAdd={onRowAdd}
        onRowDelete={onRowDelete}
      />
    </>
  );
});
