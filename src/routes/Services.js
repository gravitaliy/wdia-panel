import React, { useEffect, useState } from "react";
import { observer } from "mobx-react";
import { useHistory } from "react-router-dom";
import { Title } from "../components/_title";
import { NewTable } from "../components/NewTable";
import { routesList } from "../assets/enums/routesList";
import { AddModal } from "../components/_popups";
import {
  getLookup,
  getValueFromObject,
  renderDate,
} from "../assets/functions";

export const Services = observer(({ store }) => {
  const { servicesStore, verifiersStore, commonResourcesStore } = store;

  const initialData = servicesStore.services;

  const history = useHistory();

  const [open, setOpen] = useState(false);
  const [selectedService, setselectedService] = useState({});

  useEffect(() => {
    servicesStore.handleGetServiceList();
    verifiersStore.handleGetVerifierList();
    commonResourcesStore.handleGetCommonResources();
  }, []);

  const tableHeadDataGeneral = [
    {
      field: "name",
      title: "Name",
      filtering: false,
    },
    {
      field: "contactEmail",
      title: "Contact e-mail",
      filtering: false,
    },
    {
      field: "contactPhone",
      title: "Contact phone",
      filtering: false,
    },
    {
      field: "description",
      title: "Description",
      filtering: false,
    },
    {
      field: "url",
      title: "URL",
      filtering: false,
    },
    {
      field: "backendUrl",
      title: "Backend URL",
      filtering: false,
    },
    {
      field: "type",
      title: "Type",
      editable: "never",
      lookup: getLookup(commonResourcesStore?.commonResources?.types?.SERVICE),
    },
    {
      field: "createdAt",
      title: "Created at",
      filtering: false,
      editable: "never",
      render: rowData => renderDate(rowData.createdAt),
    },
    {
      field: "state",
      title: "State",
      lookup: getLookup(commonResourcesStore?.commonResources?.states?.SERVICE),
    },
  ];

  const tableHeadDataModal = [
    {
      field: "name",
      title: "Name",
      filtering: false,
    },
    {
      field: "description",
      title: "Description",
      filtering: false,
    },
    {
      field: "url",
      title: "URL",
      filtering: false,
    },
  ];

  const actions = [
    {
      icon: "verified_user",
      tooltip: "Show service's verifiers",
      onClick: (event, rowData) => {
        const itemPath = `${routesList.SERVICES}/${rowData.hash}`;
        history.push(itemPath);
      },
    },
    {
      icon: "library_add",
      tooltip: "Add service to verifiers",
      onClick: (event, rowData) => {
        setselectedService(rowData);
        handleToggle();
      },
    },
  ];

  const handleToggle = () => {
    setOpen(!open);
  };

  const onRowUpdate = (newData, oldData, dataUpdate, resolve, reject) => {
    servicesStore.setServices([...dataUpdate]);

    servicesStore.handlePostUpdateService(
      getValueFromObject(newData, oldData, "hash"),
      initialData,
      resolve,
      reject
    );
  };

  const addHandle = data => {
    servicesStore.handlePostAddVerifiersToService(data);
  };

  return (
    <>
      <Title label="Services" blueGradient />
      <NewTable
        title="Services"
        columns={tableHeadDataGeneral}
        data={initialData}
        onRowUpdate={onRowUpdate}
        actions={actions}
      />
      <AddModal
        open={open}
        handleToggle={handleToggle}
        title="Verifiers"
        columns={tableHeadDataModal}
        data={verifiersStore.verifiers}
        addHandle={addHandle}
        selectedRow={selectedService}
        actionTooltip="Add service to verifiers"
        addType="service"
        disableFiltering
      />
    </>
  );
});
