import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { observer } from "mobx-react";
import { Title } from "../components/_title";
import { NewTable } from "../components/NewTable";
import {
  getLookup,
  getValueFromObject,
  renderDate,
} from "../assets/functions";
import { routesList } from "../assets/enums/routesList";

export const Companies = observer(({ store }) => {
  const { companiesListStore, commonResourcesStore } = store;

  const initialData = companiesListStore.companies;

  const history = useHistory();

  useEffect(() => {
    companiesListStore.handleGetCompaniesList();
    commonResourcesStore.handleGetCommonResources();
  }, []);

  const tableHeadData = [
    {
      field: "name",
      title: "Name",
      filtering: false,
    },
    {
      field: "contactEmail",
      title: "E-mail",
      filtering: false,
    },
    {
      field: "contactPhone",
      title: "Phone",
      filtering: false,
    },
    {
      field: "description",
      title: "Description",
      filtering: false,
    },
    {
      field: "url",
      title: "URL",
      filtering: false,
    },
    {
      field: "createdAt",
      title: "Created at",
      filtering: false,
      editable: "never",
      render: rowData => renderDate(rowData.createdAt),
    },
    {
      field: "updatedAt",
      title: "Updated at",
      filtering: false,
      editable: "never",
      render: rowData => renderDate(rowData.updatedAt),
    },
    {
      field: "state",
      title: "State",
      lookup: getLookup(commonResourcesStore?.commonResources?.states?.COMPANY),
    },
    {
      field: "type",
      title: "Type",
      lookup: getLookup(commonResourcesStore?.commonResources?.types?.COMPANY),
    },
  ];

  const onRowUpdate = (newData, oldData, dataUpdate, resolve, reject) => {
    companiesListStore.setCompanies([...dataUpdate]);

    companiesListStore.handlePostCompanyUpdate(
      getValueFromObject(newData, oldData, "id"),
      initialData,
      resolve,
      reject
    );
  };

  const actions = [
    {
      icon: "people",
      tooltip: "Show company's users",
      onClick: (event, rowData) => {
        const itemPath = `${routesList.COMPANIES}/${rowData.id}`;
        history.push(itemPath);
      },
    },
  ];

  return (
    <>
      <Title label="Companies" blueGradient />
      <NewTable
        title="Companies"
        columns={tableHeadData}
        data={initialData}
        onRowUpdate={onRowUpdate}
        actions={actions}
      />
    </>
  );
});
