import React, { useEffect } from "react";
import { observer } from "mobx-react";
import { useRouteMatch } from "react-router-dom";
import { NewTable } from "../components/NewTable";
import { routesList } from "../assets/enums/routesList";
import { TitleWithBack } from "../components/_title";

export const ServiceVerifiers = observer(({ store }) => {
  const { serviceVerifiersStore } = store;

  const match = useRouteMatch();

  useEffect(() => {
    serviceVerifiersStore.handleGetServiceVerifiers(match.params.hash);
    serviceVerifiersStore.handleGetServiceByHash(match.params.hash);
  }, []);

  const tableHeadData = [
    {
      field: "name",
      title: "Name",
      filtering: false,
    },
    {
      field: "description",
      title: "Description",
      filtering: false,
    },
    {
      field: "url",
      title: "URL",
      filtering: false,
    },
  ];

  return (
    <>
      <TitleWithBack
        backLink={routesList.SERVICES}
        label={`Service verifiers - ${serviceVerifiersStore.service.name ||
          ""}`}
      />
      <NewTable
        title="Service verifiers"
        columns={tableHeadData}
        data={serviceVerifiersStore.serviceVerifiers}
      />
    </>
  );
});
