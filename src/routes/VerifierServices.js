import React, { useEffect } from "react";
import { observer } from "mobx-react";
import { useRouteMatch } from "react-router-dom";
import { NewTable } from "../components/NewTable";
import { routesList } from "../assets/enums/routesList";
import { TitleWithBack } from "../components/_title";
import { getLookup } from "../assets/functions";

export const VerifierServices = observer(({ store }) => {
  const { verifierServicesStore, commonResourcesStore } = store;

  const match = useRouteMatch();

  useEffect(() => {
    verifierServicesStore.handleGetVerifierServices(match.params.hash);
    verifierServicesStore.handleGetVerifierByHash(match.params.hash);
    commonResourcesStore.handleGetCommonResources();
  }, []);

  const tableHeadData = [
    {
      field: "name",
      title: "Name",
      filtering: false,
    },
    {
      field: "description",
      title: "Description",
      filtering: false,
    },
    {
      field: "url",
      title: "URL",
      filtering: false,
    },
    {
      field: "type",
      title: "Type",
      editable: "never",
      lookup: getLookup(commonResourcesStore?.commonResources?.types?.SERVICE),
    },
  ];

  return (
    <>
      <TitleWithBack
        backLink={routesList.VERIFIERS}
        label={`Verifiers services - ${verifierServicesStore.verifier.name ||
          ""}`}
      />
      <NewTable
        title="Verifiers services"
        columns={tableHeadData}
        data={verifierServicesStore.verifierServices}
      />
    </>
  );
});
