import React, { useEffect } from "react";
import { observer } from "mobx-react";
import { useRouteMatch } from "react-router-dom";
import { NewTable } from "../components/NewTable";
import { routesList } from "../assets/enums/routesList";
import { TitleWithBack } from "../components/_title";

export const FieldTemplateFields = observer(({ store }) => {
  const { fieldsTemplatesStore } = store;

  const initialData = fieldsTemplatesStore.activeFields;

  const match = useRouteMatch();

  useEffect(() => {
    fieldsTemplatesStore.handleGetFieldsTemplateList(Number(match.params.id));
    fieldsTemplatesStore.handleGetFieldsList();
  }, []);

  const lookup = Object.assign({},
    ...fieldsTemplatesStore.fieldList.map(({ id, name }) => ({
      [id]: name,
    })));

  const tableHeadData = [
    {
      field: "id",
      title: "Field",
      lookup,
    },
    {
      field: "description",
      title: "Description",
      editable: "never",
    },
    {
      field: "type",
      title: "Type",
      editable: "never",
    },
  ];

  const onRowAdd = (newData, dataAdd, resolve, reject) => {
    const findField = fieldsTemplatesStore.fieldList.find(x => {
      return x.id === Number(newData.id);
    });

    fieldsTemplatesStore.setActiveFields([
      ...fieldsTemplatesStore.activeFields,
      findField,
    ]);

    const newFields = [...fieldsTemplatesStore.activeFields].map(item => item.id);

    const newDataSend = {
      id: match.params.id,
      fields: [...newFields],
    };

    fieldsTemplatesStore.handlePostFildsTemplateUpdate(
      newDataSend,
      initialData,
      resolve,
      reject
    );
  };

  const onRowDelete = (oldData, dataDelete, resolve, reject) => {
    fieldsTemplatesStore.setActiveFields([...dataDelete]);

    const newFields = [...dataDelete].map(item => item.id);

    const newData = {
      id: match.params.id,
      fields: newFields,
    };

    fieldsTemplatesStore.handlePostFildsTemplateUpdate(
      newData,
      initialData,
      resolve,
      reject
    );
  };

  return (
    <>
      <TitleWithBack
        backLink={routesList.FIELDS_TEMPLATES}
        label={`Field template fields - ${fieldsTemplatesStore.activeName}`}
      />
      <NewTable
        title="Field template fields"
        columns={tableHeadData}
        data={initialData}
        onRowAdd={onRowAdd}
        onRowDelete={onRowDelete}
        disableFiltering
      />
    </>
  );
});
