import React from "react";
import Loader from "react-loader-spinner";
import { usePromiseTracker } from "react-promise-tracker";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { Box } from "@material-ui/core";

export const MyLoader = () => {
  const { promiseInProgress } = usePromiseTracker();

  return (
    promiseInProgress && (
      <Box
        style={{
          position: "absolute",
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          zIndex: 9999,
          display: "flex",
          justifyContent: "center",
          backgroundColor: "#ffffff",
        }}
      >
        <Box
          style={{
            position: "fixed",
            top: 0,
            height: "100vh",
            display: "flex",
            alignItems: "center",
          }}
        >
          <Loader type="Oval" color="#00BFFF" height={100} width={100} />
        </Box>
      </Box>
    )
  );
};
