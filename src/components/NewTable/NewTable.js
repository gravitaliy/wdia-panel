import React, { forwardRef } from "react";
import MaterialTable from "material-table";
import { AddBox, DeleteOutline } from "@material-ui/icons";

export const NewTable = props => {
  const options = {
    headerStyle: {
      fontWeight: "bold",
    },
    pageSize: 10,
    filtering: !props.disableFiltering,
    selection: props.selection,
    pageSizeOptions: [5, 10, 20, 50, 100],
  };

  const icons = {
    Add: forwardRef((props, ref) => (
      <AddBox
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...props}
        ref={ref}
        color="primary"
        fontSize="large"
      />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...props}
        ref={ref}
        color="error"
      />
    )),
  };

  const onCellEditApproved = (newValue, oldValue, rowData, columnDef) =>
    new Promise((resolve, reject) => {
      const newData = Object.assign({},
        ...Object.entries(rowData).map(([key, value]) => {
          if (columnDef.field === key) { return { [key]: newValue } }
          return { [key]: value }
        }));

      const dataUpdate = [...props.data];
      const index = rowData.tableData.id;
      dataUpdate[index] = newData;

      props.cellEditable(
        newValue,
        rowData,
        columnDef,
        dataUpdate,
        resolve,
        reject
      );
    });

  const onRowUpdate = (newData, oldData) =>
    new Promise((resolve, reject) => {
      const dataUpdate = [...props.data];
      const index = oldData.tableData.id;
      dataUpdate[index] = newData;

      props.onRowUpdate(newData, oldData, dataUpdate, resolve, reject);
    });

  const onRowAdd = newData =>
    new Promise((resolve, reject) => {
      const dataAdd = [...props.data, newData];
      props.onRowAdd(newData, dataAdd, resolve, reject);
    });

  const onRowDelete = oldData =>
    new Promise((resolve, reject) => {
      const dataDelete = [...props.data];
      const index = oldData.tableData.id;
      dataDelete.splice(index, 1);

      props.onRowDelete(oldData, dataDelete, resolve, reject);
    });

  const onRowClick = (event, rowData, togglePanel) => {
    return togglePanel();
  };

  return (
    <MaterialTable
      title={props.title}
      columns={props.columns}
      data={props.data}
      options={options}
      editable={{
        onRowUpdate: props.onRowUpdate && onRowUpdate,
        onRowAdd: props.onRowAdd && onRowAdd,
        onRowDelete: props.onRowDelete && onRowDelete,
      }}
      cellEditable={
        props.cellEditable && {
          onCellEditApproved,
        }
      }
      actions={props.actions}
      onRowClick={props.detailPanelData && onRowClick}
      icons={icons}
    />
  );
};
