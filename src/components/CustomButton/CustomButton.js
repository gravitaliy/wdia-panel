import React from "react";
import { NavLink } from "react-router-dom";
import clsx from "clsx";
import { makeStyles, ButtonBase, CircularProgress } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  button: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    textDecoration: "none",
    width: "100%",
    borderRadius: 8,
    transition: "opacity 0.2s ease-out",
    textTransform: "none",
    "&:hover": {
      opacity: 0.7,
    },
    "&:disabled": {
      opacity: 0.5,
    },
  },
  button_green: {
    background: "rgba(59,211,157, 0.1)",
    color: "#3BD39D",
  },
  button_blue: {
    background:
      "linear-gradient(90deg, rgba(0,153,255,0.1) 0%, rgba(16,83,255,0.1) 78.65%)",
    color: "#1053FF",
  },
  button_darkBlue: {
    background: "linear-gradient(90deg, #0099FF 0%, #1053FF 78.65%)",
    color: "#ffffff",
  },
  button_outline_blue: {
    border: "1px solid #1053FF",
    background: "transparent",
    color: "#1053FF",
  },
  button_outline_grey: {
    border: "1px solid rgba(81,81,81, 0.7)",
    background: "transparent",
    color: "rgba(81,81,81, 0.7)",
  },
  circle_white: {
    color: "#ffffff",
  },
  circle_blue: {
    color: "#1053FF",
  },
  circle_green: {
    color: "#3BD39D",
  },
  button_md: {
    maxWidth: 140,
    height: 36,
  },
  button_lg: {
    maxWidth: 154,
    height: 46,
  },
  button_max: {
    height: 60,
    fontSize: 16,
    fontWeight: "bold",
  },
  button_xlg: {
    maxWidth: 340,
    height: 80,
    fontSize: 20,
    fontWeight: "bold",
    [theme.breakpoints.down("sm")]: {
      height: 60,
    },
  },
  "button_xlg-max": {
    height: 80,
    fontSize: 20,
    fontWeight: "bold",
    [theme.breakpoints.down("sm")]: {
      height: 60,
    },
  },
}));

export const CustomButton = props => {
  const classes = useStyles();

  const classColor = () => {
    if (props.outline) {
      return classes[`button_outline_${props.color}`];
    }
    return classes[`button_${props.color}`] || classes.button_blue;
  };

  const classCircle = () => {
    return classes[`circle_${props.color}`];
  };

  const classSize = () => {
    return classes[`button_${props.size}`] || classes.button_md;
  };

  return props.link ? (
    <NavLink
      className={clsx(
        props.className,
        classes.button,
        classColor(),
        classSize()
      )}
      to={props.link}
    >
      {props.children}
    </NavLink>
  ) : (
    <ButtonBase
      type={props.submit && "submit"}
      className={clsx(
        props.className,
        classes.button,
        classColor(),
        classes[`button_${props.size}`]
      )}
      onClick={props.onClick}
      disabled={props.disabledIsPending}
    >
      {props.disabledIsPending ? (
        <CircularProgress className={classCircle()} />
      ) : (
        props.children
      )}
    </ButtonBase>
  );
};
