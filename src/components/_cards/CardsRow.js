import React from "react";
import { Box, makeStyles } from "@material-ui/core";
import clsx from "clsx";

const useStyles = makeStyles(theme => ({
  row: {
    display: "flex",
    flexWrap: "wrap",
    margin: "0 -15px 30px",
    [theme.breakpoints.down("md")]: {
      flexDirection: "column",
      marginBottom: 0,
    },
  },
}));

export const CardsRow = ({ children, className }) => {
  const classes = useStyles();

  return <Box className={clsx(className, classes.row)}>{children}</Box>;
};
