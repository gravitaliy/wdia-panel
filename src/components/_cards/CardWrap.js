import React from "react";
import clsx from "clsx";
import { Box, makeStyles } from "@material-ui/core";
import { observer } from "mobx-react";

const useStyles = makeStyles(theme => ({
  col: {
    position: "relative",
    boxSizing: "border-box",
    display: "flex",
    flexDirection: "column",
    overflow: "hidden",
    margin: "0 15px",
    padding: "40px 30px",
    background: "#FFFFFF",
    boxShadow: "0 30px 60px rgba(138,149,158, 0.2)",
    borderRadius: 8,
    color: "#515151",
    [theme.breakpoints.down("md")]: {
      marginTop: 15,
      marginBottom: 15,
    },
  },
  col_sm: {
    flexBasis: "18.7%",
    minWidth: 215,
  },
  col_md: {
    flexBasis: "28.8%",
  },
  col_lg: {
    flexBasis: "39%",
  },
  col_half: {
    flexBasis: "calc(50% - 30px)",
  },
  col_grow: {
    flex: 1,
  },
}));

export const CardWrap = observer(({ children, size, style, className }) => {
  const classes = useStyles();

  const classSize = () => {
    return classes[`col_${size}`];
  };

  return (
    <Box className={clsx(className, classes.col, classSize())} style={style}>
      {children}
    </Box>
  );
});
