import React from "react";
import { Link } from "react-router-dom";
import { Box } from "@material-ui/core";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import { Title } from "./Title";

export const TitleWithBack = ({ backLink, label }) => {
  return (
    <Box style={{ display: "flex" }}>
      <Link to={backLink}>
        <ArrowBackIosIcon
          fontSize="large"
          style={{
            marginTop: 15,
            marginRight: 20,
            color: "#0099FF",
          }}
        />
      </Link>
      <Title label={label} blueGradient />
    </Box>
  );
};
