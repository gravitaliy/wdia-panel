import React from "react";
import { Box, makeStyles } from "@material-ui/core";
import clsx from "clsx";

const useStyles = makeStyles(theme => ({
  label: {
    marginBottom: 40,
    fontSize: 56,
    lineHeight: 1.15,
    fontWeight: "bold",
    color: "#515151",
    [theme.breakpoints.down("sm")]: {
      fontSize: 40,
    },
  },
  blueGradient: {
    background: "linear-gradient(90deg, #0099FF 0%, #1053FF 78.65%)",
    "-webkit-background-clip": "text",
    textFillColor: "transparent",
    color: "#1053FF",
  },
}));

export const Title = ({ label, blueGradient, fontSize, className }) => {
  const classes = useStyles();

  return (
    <Box
      className={clsx(
        classes.label,
        blueGradient && classes.blueGradient,
        className
      )}
      style={{ fontSize }}
    >
      {label}
    </Box>
  );
};
