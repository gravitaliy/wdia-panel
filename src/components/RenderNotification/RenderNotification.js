import React from "react";
import MuiAlert from "@material-ui/lab/Alert";
import { Snackbar } from "@material-ui/core";
import { observer } from "mobx-react";

export const RenderNotification = observer(({ store }) => {
  const { notification } = store;
  const { open, message, type, duration } = notification;

  const Alert = props => {
    return (
      <MuiAlert
        // eslint-disable-next-line
        {...props}
        elevation={6}
        variant="filled"
      />
    );
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    store.setNotification({
      open: false,
    });
  };

  return (
    <Snackbar open={open} autoHideDuration={duration} onClose={handleClose}>
      <Alert onClose={handleClose} severity={type}>
        {message}
      </Alert>
    </Snackbar>
  );
});
