import React, { forwardRef } from "react";
import { Modal, IconButton, Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { AddBox } from "@material-ui/icons";
import CloseIcon from "@material-ui/icons/Close";
import { NewTable } from "../NewTable";

const useStyles = makeStyles(() => ({
  paper: {
    position: "absolute",
    top: "50%",
    left: "50%",
    width: "90%",
    maxHeight: "90vh",
    overflowY: "auto",
    transform: "translate(-50%, -50%)",
    backgroundColor: "#ffffff",
  },
  closeButton: {
    display: "flex",
    margin: "8px 8px 8px auto",
  },
}));

export const AddModal = props => {
  const classes = useStyles();

  const actionsModal = [
    {
      icon: forwardRef((props, ref) => (
        <AddBox
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...props}
          ref={ref}
          color="primary"
          fontSize="large"
        />
      )),
      tooltip: props.actionTooltip,
      onClick: (event, rowData) => {
        const dependentHashes = [...rowData].map(item => item.hash);

        const data = {
          type: props.addType,
          hash: props.selectedRow.hash,
          dependentHashes,
        };

        props.addHandle(data);

        props.handleToggle();
      },
    },
  ];

  return (
    <Modal open={props.open} onClose={props.handleToggle}>
      <Box className={classes.paper}>
        {props.handleToggle && (
          <IconButton
            className={classes.closeButton}
            onClick={props.handleToggle}
          >
            <CloseIcon />
          </IconButton>
        )}
        <NewTable
          title={props.title}
          columns={props.columns}
          data={props.data}
          actions={actionsModal}
          selection
          filtering={props.filtering}
        />
      </Box>
    </Modal>
  );
};
