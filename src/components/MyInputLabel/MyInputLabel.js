import React, { useState } from "react";
import {
  FormLabel,
  InputAdornment,
  makeStyles,
  TextField,
  IconButton,
  Box,
} from "@material-ui/core";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import clsx from "clsx";
import { emailRegex, phoneRegex } from "../../assets/regExp/regExp";
import { emailError, phoneError } from "../../assets/regExp/regExpErrors";

const useStyles = makeStyles({
  textInputWrap: {
    marginTop: 50,
  },
  labelRoot: {
    display: "block",
    marginBottom: 15,
    fontSize: 20,
    textTransform: "uppercase",
    color: "#515151",
  },
  textarea: {
    borderTop: "none",
    resize: "none",
  },
  iconShowPassword: {
    marginBottom: 3,
    color: "#515151",
    opacity: 0.3,
    fontSize: "1rem",
  },
});

export const MyInputLabel = props => {
  const classes = useStyles();

  const [showPassword, setShowPassword] = useState(false);

  const setItem = e => {
    props.setItem(e.target.name, e.target.value);
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };

  return (
    <Box
      className={clsx(
        classes.textInputWrap,
        props.classes && props.classes.root
      )}
    >
      <FormLabel
        classes={{
          root: classes.labelRoot,
        }}
      >
        {props.label}
      </FormLabel>
      <TextField
        required={props.required}
        autoFocus={props.autoFocus}
        multiline={props.type === "textarea"}
        margin="dense"
        fullWidth
        type={
          // eslint-disable-next-line
          props.type === "password"
            ? showPassword
              ? "text"
              : "password"
            : props.type
        }
        placeholder={props.placeholder}
        value={props.type === "email" ? props.value.toLowerCase() : props.value}
        onChange={setItem}
        name={props.name}
        InputProps={{
          pattern: props.pattern || ".*",
          title: props.title || "",
          startAdornment: props.iconBefore && (
            <InputAdornment position="start">{props.iconBefore}</InputAdornment>
          ),
          endAdornment: props.type === "password" && (
            <InputAdornment position="end">
              <IconButton
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
                edge="end"
              >
                {showPassword ? (
                  <Visibility className={classes.iconShowPassword} />
                ) : (
                  <VisibilityOff className={classes.iconShowPassword} />
                )}
              </IconButton>
            </InputAdornment>
          ),
        }}
        error={props.error}
        helperText={props.helperText}
        // eslint-disable-next-line
        inputProps={{
          pattern:
            (props.type === "email" && emailRegex) ||
            (props.type === "tel" && phoneRegex) ||
            props.pattern,
          title:
            (props.type === "email" && emailError) ||
            (props.type === "tel" && phoneError) ||
            props.title,
        }}
      />
    </Box>
  );
};
