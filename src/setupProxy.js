const { createProxyMiddleware } = require("http-proxy-middleware");

function proxy(app) {
  app.use(
    "/v1",
    createProxyMiddleware({
      target: "https://api-dev.wdia.org",
      changeOrigin: true,
    })
  );
}

module.exports = proxy;
