import React from "react";
import { Box, CssBaseline } from "@material-ui/core";
import { HeaderAuth } from "../HeaderAuth";
import { Footer } from "../Footer";

export const LayoutAuth = ({ children }) => {
  return (
    <>
      <CssBaseline />
      <Box>
        <HeaderAuth />
        {children}
        <Footer />
      </Box>
    </>
  );
};
