import React, { useEffect } from "react";
import { Box, CssBaseline, makeStyles } from "@material-ui/core";
import { observer } from "mobx-react";
import { HeaderLK } from "../HeaderLK";
import { MyLoader } from "../../components/MyLoader";

const headerHeight = {
  lg: 90,
  sm: 83,
};

const useStyles = makeStyles(theme => ({
  totalWrap: {
    display: "flex",
  },
  content: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
    background:
      "linear-gradient(90deg, rgba(0,123,255, 0.04) 0%, rgba(16,83,255, 0.04) 78.65%)",
  },
  appBarSpacer: {
    minHeight: headerHeight.lg,
    [theme.breakpoints.down("sm")]: {
      padding: headerHeight.sm,
    },
  },
  container: {
    position: "relative",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    padding: 60,
    // flexGrow: 1,
    [theme.breakpoints.down("sm")]: {
      padding: 15,
    },
  },
}));

export const LayoutHome = observer(({ children, store }) => {
  const classes = useStyles();

  useEffect(() => {
    store.userDataStore.handleGetUserData();
  }, []);

  return (
    <>
      <CssBaseline />
      <Box className={classes.totalWrap}>
        <HeaderLK headerHeight={headerHeight} store={store} />
        <main className={classes.content}>
          <Box className={classes.appBarSpacer} />
          <Box className={classes.container}>
            {children}
            <MyLoader />
          </Box>
        </main>
      </Box>
    </>
  );
});
