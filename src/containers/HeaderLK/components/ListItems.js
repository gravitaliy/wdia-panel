import React from "react";
import { NavLink } from "react-router-dom";
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  makeStyles,
} from "@material-ui/core";
import { observer } from "mobx-react";
import clsx from "clsx";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import { routesList } from "../../../assets/enums/routesList";

const useStyles = makeStyles(() => ({
  iconWidth: {
    minWidth: "auto",
  },
  iconWrapShift: {
    justifyContent: "center",
  },
  listText: {
    marginLeft: 10,
    marginTop: 0,
    marginBottom: 0,
  },
  iconToggleWrap: {
    marginTop: "100px",
  },
  iconToggle: {
    color: "#ffffff",
    opacity: 0.5,
  },
  iconToggleClose: {
    transform: "rotate(180deg)",
  },
  navLink: {
    display: "block",
    color: "inherit",
    textDecoration: "inherit",
    transition: "background-color 0.2s ease-out",
    "&:hover": {
      backgroundColor: "rgba(81,81,81, 0.2)",
    },
  },
  navLinkActive: {
    backgroundColor: "rgba(81,81,81, 0.2)",
  },
  listItem: {
    "&:hover": {
      backgroundColor: "inherit",
      textDecoration: "inherit",
    },
  },
}));

export const ListItems = observer(({ open, onOpen }) => {
  const classes = useStyles();

  const listItemsData = [
    {
      link: routesList.COMPANIES,
      icon: require("./assets/company.svg"),
      label: "Companies",
    },
    {
      link: routesList.SERVICES,
      icon: require("./assets/services.svg"),
      label: "Services",
    },
    {
      link: routesList.VERIFIERS,
      icon: require("./assets/services.svg"),
      label: "Verifiers",
    },
    {
      link: routesList.QUESTIONS,
      icon: require("./assets/users.svg"),
      label: "Questions",
    },
    {
      link: routesList.FIELDS_TEMPLATES,
      icon: require("./assets/profile.svg"),
      label: "Fields templates",
    },
/*
    {
      link: routesList.ROLES,
      icon: require("./assets/profile.svg"),
      label: "Roles",
    },
*/
  ].map((item, i) => ({ ...item, id: i }));

  return (
    <List>
      {listItemsData.map(item => (
        <NavLink
          exact={item.id === 0}
          className={classes.navLink}
          activeClassName={classes.navLinkActive}
          to={item.link}
          key={item.id}
        >
          <ListItem
            button
            className={clsx(classes.iconWrapShift, classes.listItem)}
          >
            <ListItemIcon className={classes.iconWidth}>
              <img src={item.icon} alt="" />
            </ListItemIcon>
            {open && (
              <ListItemText primary={item.label} className={classes.listText} />
            )}
          </ListItem>
        </NavLink>
      ))}
      <ListItem
        button
        onClick={onOpen}
        className={clsx(classes.iconWrapShift, classes.iconToggleWrap)}
      >
        <ListItemIcon className={classes.iconWidth}>
          <ChevronLeftIcon
            className={clsx(
              classes.iconToggle,
              !open && classes.iconToggleClose
            )}
          />
        </ListItemIcon>
        {open && (
          <ListItemText
            primary="Minimize"
            className={clsx(classes.iconToggle, classes.listText)}
          />
        )}
      </ListItem>
    </List>
  );
});
