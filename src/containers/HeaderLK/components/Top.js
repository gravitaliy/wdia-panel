import React from "react";
import {
  AppBar,
  makeStyles,
  Toolbar,
  Typography,
  ButtonBase,
  Box,
} from "@material-ui/core";
import { observer } from "mobx-react";
import clsx from "clsx";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { CookieHelper, COOKIES } from "../../../assets/functions";
import {routesList} from "../../../assets/enums/routesList";

export const Top = observer(({ open, store, drawerWidth, headerHeight }) => {
  const useStyles = makeStyles(theme => ({
    appBar: {
      backgroundColor: "#ffffff",
      color: "#515151",
      boxShadow: "none",
      zIndex: theme.zIndex.drawer + 1,
      width: `calc(100% - ${drawerWidth.close}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      [theme.breakpoints.down("sm")]: {
        width: `calc(100% - ${drawerWidth.closeSm}px)`,
      },
    },
    appBarShift: {
      marginLeft: drawerWidth.open,
      width: `calc(100% - ${drawerWidth.open}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
      [theme.breakpoints.down("sm")]: {
        marginLeft: drawerWidth.openSm,
        width: `calc(100% - ${drawerWidth.openSm}px)`,
      },
    },
    toolbar: {
      padding: "0 60px",
      minHeight: headerHeight.lg,
      [theme.breakpoints.down("sm")]: {
        flexWrap: "wrap",
        justifyContent: "center",
        paddingLeft: 15,
        paddingRight: 15,
        minHeight: headerHeight.sm,
      },
    },
    company: {
      display: "flex",
      alignItems: "center",
      marginLeft: "auto",
      [theme.breakpoints.down("sm")]: {
        marginLeft: 0,
        marginBottom: 10,
        flexBasis: "100%",
        justifyContent: "center",
      },
      [theme.breakpoints.down("xs")]: {
        flexBasis: "60%",
      },
    },
    companyLabel: {
      fontSize: 14,
    },
    logoutWrap: {
      marginLeft: 15,
      transition: "opacity 0.2s ease-out",
      "&:hover": {
        opacity: 0.7,
      },
    },
  }));
  const classes = useStyles();

  const handleLogout = () => {
    CookieHelper.remove(COOKIES.AUTHORIZATION);
    window.location.href = routesList.SIGN_IN;
  };

  return (
    <AppBar
      position="absolute"
      className={clsx(classes.appBar, open && classes.appBarShift)}
    >
      <Toolbar className={classes.toolbar}>
        <Box className={classes.company}>
          <Typography className={classes.companyLabel}>
            {store.userDataStore.user.email || "loading..."}
          </Typography>
          <ButtonBase className={classes.logoutWrap} onClick={handleLogout}>
            <ExitToAppIcon />
          </ButtonBase>
        </Box>
      </Toolbar>
    </AppBar>
  );
});
