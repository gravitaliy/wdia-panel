import React from "react";
import clsx from "clsx";
import { Box, Drawer, makeStyles } from "@material-ui/core";
import { ListItems } from "./ListItems";

export const Left = ({ open, onOpen, drawerWidth }) => {
  const useStyles = makeStyles(theme => ({
    toolbarHeader: {
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      padding: "20px 15px",
      ...theme.mixins.toolbar,
    },
    toolbarHeaderImg: {
      display: "block",
      maxWidth: "100%",
      height: "auto",
    },
    drawerPaper: {
      position: "relative",
      whiteSpace: "nowrap",
      backgroundColor: "#0C1132",
      color: "#ffffff",
      width: drawerWidth.open,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
      [theme.breakpoints.down("sm")]: {
        width: drawerWidth.openSm,
      },
    },
    drawerPaperClose: {
      overflowX: "hidden",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      width: theme.spacing(7),
      [theme.breakpoints.up("sm")]: {
        width: drawerWidth.close,
      },
      [theme.breakpoints.down("sm")]: {
        width: drawerWidth.closeSm,
      },
    },
  }));
  const classes = useStyles();

  return (
    <Drawer
      variant="permanent"
      classes={{
        paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
      }}
      open={open}
    >
      <Box className={classes.toolbarHeader}>
        <Box>
          <img
            className={classes.toolbarHeaderImg}
            src={require("../../../assets/images/logo.svg")}
            alt="logo"
          />
        </Box>
      </Box>
      <ListItems open={open} onOpen={onOpen} />
    </Drawer>
  );
};
