import React, { useState } from "react";
import { Top, Left } from "./components";

const drawerWidth = {
  open: 265,
  close: 148,
  openSm: 180,
  closeSm: 100,
};

export const HeaderLK = ({ headerHeight, store }) => {
  const defaultOpen = document.documentElement.clientWidth >= 600;

  const [open, setOpen] = useState(defaultOpen);

  const handleDrawerToggle = () => {
    setOpen(!open);
  };

  return (
    <>
      <Top
        store={store}
        open={open}
        drawerWidth={drawerWidth}
        headerHeight={headerHeight}
      />
      <Left open={open} onOpen={handleDrawerToggle} drawerWidth={drawerWidth} />
    </>
  );
};
