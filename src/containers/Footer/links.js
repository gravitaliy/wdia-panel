import { routesList } from "../../assets/enums/routesList";

/* ====================== LINKS ICONS ======================== */

export const linksIcons = [
  {
    icon: require("./assets/icons/1.svg"),
    link: "mailto:contact@wdia.org",
  },
  {
    icon: require("./assets/icons/2.svg"),
    link: "https://twitter.com/wdia_org",
  },
  {
    icon: require("./assets/icons/3.svg"),
    link: "https://t.me/wdia_org",
  },
  {
    icon: require("./assets/icons/5.svg"),
    link:
      "https://www.meetup.com/ru-RU/The-Worldwide-Digital-Identification-Association-WDIA/members/298332190/",
  },
  {
    icon: require("./assets/icons/6.svg"),
    link: "https://www.instagram.com/wdiaorg/",
  },
  {
    icon: require("./assets/icons/7.svg"),
    link: "https://medium.com/@wdia.org",
  },
  {
    icon: require("./assets/icons/8.svg"),
    link: "https://www.facebook.com/wdia.org",
  },
  {
    icon: require("./assets/icons/9.svg"),
    link: "https://www.linkedin.com/company/wdia/",
  },
].map((item, i) => ({ ...item, id: i }));

/* ====================== LINKS MENU ======================== */

export const linksMenu = () => {
  return [
    {
      label: "Our activities",
      link: routesList.OUR_ACTIVITIES,
      blank: false,
    },
    {
      label: "Login",
      link: routesList.SIGN_IN,
      blank: false,
    },
    {
      label: "Donate",
      link: "",
      blank: false,
    },
    {
      label: "Privacy Policy",
      link: require("./assets/WDIA_Privacy_Policy.pdf"),
      blank: true,
    },
    {
      label: "Terms of use",
      link: require("./assets/WDIA_Terms_Use.pdf"),
      blank: true,
    },
  ].map((item, i) => ({ ...item, id: i }));
};

/* ====================== LINKS ABOUT US ======================== */

export const linksAboutUs = () => {
  return [
    {
      label: "About our technology",
      href: routesList.ABOUT_TECH,
    },
    {
      label: "About association",
      href: routesList.ABOUT_US,
    },
    {
      label: "Membership",
      href: routesList.MEMBERSHIP,
    },
    {
      label: "Our Network",
      href: routesList.OUR_NETWORK,
    },
    {
      label: "Contacts",
      href: routesList.CONTACTS,
    },
  ].map((item, i) => ({ ...item, id: i }));
};
