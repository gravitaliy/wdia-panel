import React from "react";
import clsx from "clsx";
import { Box } from "@material-ui/core";
import { Title } from "../components";
import { linksMenu, linksAboutUs, linksIcons } from "./links";
import { routesList } from "../../assets/enums/routesList";
import "./Footer.scss";

export const Footer = () => {
  return (
    <footer className="footer">
      <Box className="c-container">
        <Box className="footer-list">
          <Box
            className={clsx("footer-item", "footer-info")}
            style={{ paddingTop: 30 }}
          >
            <a href={routesList.SIGN_IN}>
              <img
                className="footer-logo"
                src={require("./assets/logo-white.svg")}
                alt="logo"
              />
            </a>
            <p className="copyright">© 2020 WDIA</p>
          </Box>
          <Box className="footer-links">
            <Box className="footer-item">
              <Title className="footer-item-title">ABOUT US</Title>
              <ul className="footer-list-links">
                {linksAboutUs().map(item => (
                  <li key={item.id}>
                    <a href={item.href}>{item.label}</a>
                  </li>
                ))}
              </ul>
            </Box>
            <Box className="footer-item">
              <Title className="footer-item-title">MENU</Title>
              <ul className="footer-list-links">
                {linksMenu().map(item => (
                  <li key={item.id}>
                    <a
                      target={item.blank ? "_blank" : ""}
                      rel="noopener noreferrer"
                      href={item.link}
                    >
                      {item.label}
                    </a>
                  </li>
                ))}
              </ul>
            </Box>
          </Box>
          <Box className={clsx("footer-item", "footer-socials")}>
            <Box>
              <Title className="footer-item-title">CONNECT</Title>
              <Box className="footer-list-icons">
                {linksIcons.map(item => (
                  <a
                    href={item.link}
                    target="_blank"
                    rel="noopener noreferrer"
                    key={item.id}
                  >
                    <img src={item.icon} alt="icon" />
                  </a>
                ))}
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </footer>
  );
};
