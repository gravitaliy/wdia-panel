import React, { useState } from "react";
import clsx from "clsx";
import Dropdown from "rc-dropdown";
import { Box } from "@material-ui/core";
import { routesList } from "../../assets/enums/routesList";
import { Button } from "../components";
import "rc-dropdown/assets/index.css";
import "./HeaderAuth.scss";

export const HeaderAuth = () => {
  const [isOpen, setIsOpen] = useState(false);

  const navbar = [
    {
      label: "About us",
      child: [
        {
          label: "About our Technology",
          link: routesList.ABOUT_TECH,
        },
        {
          label: "About association",
          link: routesList.ABOUT_US,
        },
      ],
    },
    {
      label: "Our activities",
      link: routesList.OUR_ACTIVITIES,
    },
    {
      label: "Contacts",
      link: routesList.CONTACTS,
    },
  ].map((item, i) => ({ ...item, id: i }));

  const onClickNavItem = e => {
    if (isOpen && e.target.id !== "js-navbar-item-with-child") {
      toggleOpenMenu();
    }
  };

  const toggleOpenMenu = () => {
    setIsOpen(!isOpen);
  };

  return (
    <header className="navbar-wrap">
      <Box className="c-container">
        <Box
          className={clsx("navbar", "flex", "justify-between", "align-center")}
        >
          <a className="navbar-icon-wrap" href={routesList.SIGN_IN}>
            <img
              className="fix-img"
              src={require("./assets/logo.svg")}
              alt="logo"
            />
          </a>

          {/* eslint-disable-next-line */}
          <Box
            onClick={e => onClickNavItem(e)}
            className={clsx("list-navbar-links", isOpen && "open")}
          >
            <Box className="list-links">
              {navbar.map(item => {
                const menu = () => (
                  <Box className="navbar-child-items" key={item.id}>
                    {item.child.map((child, i) => (
                      <Box className="navbar-child-items-item" key={i}>
                        <a
                          className={clsx("navbar-item", "navbar-link")}
                          href={child.link}
                        >
                          {child.label}
                        </a>
                      </Box>
                    ))}
                  </Box>
                );

                return item.child ? (
                  <Dropdown
                    overlay={menu}
                    trigger={["click"]}
                    animation="slide-up"
                    key={item.id}
                  >
                    <span
                      className={clsx("navbar-item", "navbar-item-with-child")}
                      id="js-navbar-item-with-child"
                    >
                      {item.label}
                    </span>
                  </Dropdown>
                ) : (
                  <a
                    className={clsx("navbar-link", "navbar-item")}
                    href={item.link}
                    key={item.link}
                  >
                    {item.label}
                  </a>
                );
              })}
            </Box>
          </Box>

          <Box className="login-button">
            <a
              style={{ textDecoration: "none", color: "white" }}
              href={routesList.SIGN_IN}
            >
              <Button>Login</Button>
            </a>
          </Box>

          <button
            onClick={toggleOpenMenu}
            type="button"
            className="burger-menu-button"
          >
            <img src={require("./assets/burger.svg")} alt="burger" />
          </button>
        </Box>
      </Box>
    </header>
  );
};
