import React from "react";
import classNames from "classnames";
import "./Title.scss";

export const Title = ({ children, className, style }) => (
  <h3
    className={classNames("title", { [className]: !!className })}
    style={style}
  >
    {children}
  </h3>
);
