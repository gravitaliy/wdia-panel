import React from "react";
import classNames from "classnames";
import "./Button.scss";

/* eslint-disable react/button-has-type */
export const Button = ({
  text,
  children,
  onClick,
  type,
  typeButton,
  style,
  className,
  color,
}) => (
  <button
    type={type || "button"}
    onClick={onClick}
    style={style}
    className={classNames("button button-default", {
      [typeButton]: typeButton,
      [color]: color,
      [className]: className,
    })}
  >
    {text || children}
  </button>
);
