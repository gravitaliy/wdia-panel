import React from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
import { ThemeProvider } from "@material-ui/core/styles";
import { RootStore } from "./stores/rootStore";
import { routesList } from "./assets/enums/routesList";
import { LayoutHome, LayoutAuth } from "./containers/layouts";
import { HomePage } from "./pages";
import { SignIn } from "./routes";
import { outerTheme } from "./assets/enums/theme";
import { RenderNotification } from "./components/RenderNotification";
import { CookieHelper, COOKIES } from "./assets/functions";
import "./App.scss";

const store = new RootStore();

export const App = () => {
  const auth = CookieHelper.get(COOKIES.AUTHORIZATION);

  return (
    <ThemeProvider theme={outerTheme}>
      <RenderNotification store={store.notificationStore} />
      <Router>
        <Switch>
          <Route
            exact
            path={routesList.SIGN_IN}
            title="SignIn"
            component={() => {
              if (auth) {
                return <Redirect to={routesList.COMPANIES} />;
              }
              return (
                <LayoutAuth>
                  <SignIn store={store.signInStore} />
                </LayoutAuth>
              );
            }}
          />
          <Route
            path={routesList.HOME}
            component={() => {
              if (auth) {
                return (
                  <LayoutHome store={store}>
                    <HomePage store={store} />
                  </LayoutHome>
                );
              }
              return <Redirect to={routesList.SIGN_IN} />;
            }}
          />
        </Switch>
      </Router>
    </ThemeProvider>
  );
};
