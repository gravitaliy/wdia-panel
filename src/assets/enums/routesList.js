const wdiaSite = "https://dev.wdia.org";

export const routesList = {
  SIGN_IN: "/",
  HOME: "/home",
  COMPANIES: "/home/companies",
  SERVICES: "/home/services",
  VERIFIERS: "/home/verifiers",
  QUESTIONS: "/home/questions",
  FIELDS_TEMPLATES: "/home/fields-templates",
  ROLES: "/home/roles",

  ABOUT_US: `${wdiaSite}/about-us`,
  ABOUT_TECH: `${wdiaSite}/about-tech`,
  OUR_ACTIVITIES: `${wdiaSite}/our-activities`,
  OUR_NETWORK: `${wdiaSite}/our-network`,
  MEMBERSHIP: `${wdiaSite}/membership`,
  CONTACTS: `${wdiaSite}/join-us`,
};
