import { createMuiTheme } from "@material-ui/core/styles";

export const outerTheme = createMuiTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1500,
    },
  },
  palette: {
    primary: {
      main: "#1053FF",
    },
    secondary: {
      main: "#0099FF",
    },
  },
});
