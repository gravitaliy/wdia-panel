import cookies from "js-cookie";
import React from "react";
import moment from "moment";

export const renderDate = date => {
  const getDate = moment(date);
  return (
    <>
      {getDate.format("DD.MM.YYYY")}
      <br />
      {getDate.format("HH:mm")}
    </>
  );
};

/*
export const renderIsEmpty = field => {
  if (field === null) {
    return <span style={{ opacity: 0.5 }}>empty</span>;
  }
  return field;
};
*/

export const getValueFromObject = (newData, oldData, id) => {
  const newEntries = Object.entries(newData);
  const oldValues = Object.values(oldData);
  const newDataSend = {
    [id]: oldData[id],
  };

  newEntries.forEach((item, i) => {
    const key = item[0];
    const newValue = item[1];
    const oldValue = oldValues[i];

    if (newValue !== oldValue) {
      newDataSend[key] = newValue;
    }
  });
  return newDataSend;
};

export const getStatus = status => {
  const statuses = [
    {
      state: 0,
      type: "disabled",
      labelType: "disabled",
      color: "#FFD130",
      bgc: "rgba(255,209,48, 0.1)",
    },
    {
      state: 1,
      type: "active",
      labelType: "active",
      color: "#3BD39D",
      bgc: "rgba(59,211,157, 0.1)",
    },
    {
      state: 2,
      type: "blocked",
      labelType: "blocked",
      color: "#DB1430",
      bgc: "rgba(219,20,48, 0.1)",
    },
    {
      type: "expired",
      labelType: "expired",
      color: "#FFD130",
      bgc: "rgba(255,209,48, 0.1)",
    },
    {
      type: "error",
      labelType: "error",
      color: "#DB1430",
      bgc: "rgba(219,20,48, 0.1)",
    },
  ];

  return statuses.find(x => {
    if (typeof status === "number") {
      return x.state === status;
    }
    if (status) {
      return x.type === status;
    }
    return x.type === "error";
  });
};

export const CookieHelper = {
  get(key) {
    return cookies.get(key);
  },
  set(key, value, options = { expires: 7 }) {
    cookies.set(key, value, options);
  },
  remove(key) {
    cookies.remove(key);
  },
};

export const COOKIES = {
  AUTHORIZATION: "token",
};

export const deleteKey = (obj, prop) => {
  const {[prop]: omit, ...res} = obj;
  return res
};

export const getLookup = (obj = {}) => {
  return Object.assign({},
    ...Object.entries(obj).map(([key, value]) => ({
      [value]: key,
    })));
};
