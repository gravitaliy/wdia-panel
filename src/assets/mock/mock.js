export const mock = {
  companyUsers: [
    {
      firstname: "firstname1",
      lastname: "lastname1",
      email: "email@email.com1",
      state: 1,
      role: "ADMIN",
    },
    {
      firstname: "firstname2",
      lastname: "lastname2",
      email: "email@email.com2",
      state: 2,
      role: "USER",
    },
  ],

  roles: [
    {
      name: "ADMIN",
    },
    {
      name: "USER",
    },
  ],

  questions: [
    {
      createdAt: "2021.10.12 15:16",
      email: "email@email.com1",
      firstname: "firstname1",
      lastname: "lastname1",
      subject: "subject1",
      source: 1,
      state: 0,
    },
    {
      createdAt: "2021.10.13 12:26",
      email: "email@email.com2",
      firstname: "firstname2",
      lastname: "lastname2",
      subject: "subject2",
      source: 2,
      state: 1,
    },
  ],

  companyList: [
    {
      contactEmail: "info@bitchange.online",
      contactPhone: null,
      createdAt: "2020-06-17T12:22:54.000Z",
      description: "Cryptocurrency exchange market",
      id: 24,
      name: "Bitchange",
      state: 1,
      type: 1,
      updatedAt: "2020-09-03T14:19:13.000Z",
      url: "https://bitchange.online/",
    },
    {
      contactEmail: "info@tic.capital",
      contactPhone: null,
      createdAt: "2020-06-10T14:33:34.000Z",
      description: " TIC",
      id: 23,
      name: "TIC",
      state: 1,
      type: 1,
      updatedAt: "2020-09-01T10:36:55.000Z",
      url: "tic.wdia.org",
    },
    {
      contactEmail: "info@wdia.org",
      contactPhone: null,
      createdAt: "2020-06-10T14:26:21.000Z",
      description: "Service",
      id: 22,
      name: "Service",
      state: 1,
      type: 1,
      updatedAt: "2020-06-11T09:14:51.000Z",
      url: "dev.wdia.org",
    },
    {
      contactEmail: "info@sumsub.com",
      contactPhone: null,
      createdAt: "2020-06-04T14:06:26.000Z",
      description: "description Verificator SumSub",
      id: 13,
      name: "Sum Sub",
      state: 1,
      type: 2,
      updatedAt: "2020-08-27T12:03:18.000Z",
      url: "https://sumsub.com/",
    },
  ],

  commonResources: {
    states: {
      SERVICE: {DISABLED: 0, ACTIVE: 1, BLOCKED: 2, CLIENT_DISABLE: 3},
      COMPANY: {DISABLED: 0, ACTIVE: 1, BLOCKED: 2, CLIENT_DISABLE: 3},
      VERIFIER: {DISABLED: 0, ACTIVE: 1, BLOCKED: 2, CLIENT_DISABLE: 3},
      PUBLIC_KEY: {NEW: 0, ACTIVE: 1, DISABLED: 2, BLOCKED: 3, EXPIRED: 4, ERROR: 5},
      USER: {NOT_APPROVED: 0, APPROVED: 1, BLOCKED: 2, BLOCKED_BY_ADMIN: 3},
      QUESTION: {ACTIVE: 0, RESOLVED: 1},
      MEMBESHIP: {NOT_PROVIDED: 0, PROVIDED: 1, DECLINED: 2, REJECTED: 3},
      TARIFF: {DISABLED: 0, ACTIVE: 1}
    },
    types: {
      COMPANY: {SERVICE: 1, VERIFIER: 2},
      SERVICE: {
        CARSHARING: 1,
        INTERNET_SERVICE: 2,
        TRANSPORT: 3,
        BANK: 4,
        HOTEL: 5,
        STREAMING: 6,
        HEALTHCARE: 7,
        DELIVERY: 8,
        INFOSITE: 9,
        E_COMMERCE: 10,
        TICKETS_SHOW: 11
      },
      MEMBERSHIP: {CANDIDATE: 1, AUTHORIZED: 2, SILVER_PARTNER: 3, GOLDEN_PARTNER: 4, PLATINUM_PARTNER: 5},
      TARIFF: {OBSERVER: 1, MEMBER: 2}
    },
    sources: {QUESTION: {WDIA: 1, MULTIPASS: 2}}
  },

  services: [
    {
      type: 10,
      hash: "508812",
      state: 1,
      companyId: 24,
      name: "Bitchange",
      description: "online bank",
      contactEmail: "info@bitchange.online",
      contactPhone: null,
      url: "Bitchange.online",
      backendUrl: "https://api-provider-bitchange.wdia.org",
      createdAt: "2020-06-17T12:25:41.000Z",
      updatedAt: "2020-09-03T14:49:45.000Z"
    },
    {
      type: 1,
      hash: "770409",
      state: 1,
      companyId: 23,
      name: "TIC ",
      description: "TIC Service",
      contactEmail: "info@tic.capital",
      contactPhone: null,
      url: "https://tic.wdia.org",
      backendUrl: "https://api-provider-dev.wdia.org",
      createdAt: "2020-06-10T14:38:58.000Z",
      updatedAt: "2020-06-17T09:03:51.000Z"
    },
    {
      type: 1,
      hash: "975938",
      state: 1,
      companyId: 22,
      name: "Service",
      description: "Service",
      contactEmail: "service@wdia.org",
      contactPhone: null,
      url: "https://dev.wdia.org",
      backendUrl: "https://api-provider-dev.wdia.org",
      createdAt: "2020-06-10T14:37:56.000Z",
      updatedAt: "2020-09-01T10:39:55.000Z"
    }
  ],

  verifiers: [
    {
      hash: "106335",
      state: 1,
      companyId: 13,
      name: "Sum Sub",
      description: "sumsub",
      contactEmail: "info@sumsub.com",
      contactPhone: null,
      url: "http://sumsub.com",
      backendUrl: "https://dev-api-sumsub.wdia.org",
      createdAt: "2020-06-04T14:12:25.000Z",
      updatedAt: "2020-09-03T15:06:01.000Z"
    }
  ],

  verifierServices: [
    {
      hash: "508812",
      type: 10,
      name: "Bitchange",
      description: "online bank",
      url: "Bitchange.online",
      service_verifier: {
        createdAt: "2020-08-31T15:59:09.000Z",
        updatedAt: "2020-08-31T15:59:09.000Z",
        serviceId: 13,
        verifierId: 1
      }
    }
  ],

  fieldsTemplates: [
    {
      id: 1,
      name: "Test doc 2",
      description: "test doc",
      fields: [
        {
          id: 101,
          name: "Surname",
          description: "Local surname (in the language of MultiPass)",
          type: "String",
          field_fieldsTemplate: {
            createdAt: "2020-04-24T10:50:21.000Z",
            updatedAt: "2020-04-24T10:50:21.000Z"
          }
        },
        {
          id: 102,
          name: "Name",
          description: "Local name (in the language of MultiPass)",
          type: "String",
          field_fieldsTemplate: {
            createdAt: "2020-04-24T10:51:32.000Z",
            updatedAt: "2020-04-24T10:51:32.000Z"
          }
        },
        {
          id: 103,
          name: "DateOfBirth",
          description: "Date of birth",
          type: "Date",
          field_fieldsTemplate: {
            createdAt: "2020-04-24T10:50:21.000Z",
            updatedAt: "2020-04-24T10:50:21.000Z"
          }
        }
      ]
    }
  ],

  fields: [{
    "id": 955,
    "name": "AddFile5",
    "description": "Scanned file of additional document",
    "type": "File"
  }, {
    "id": 954,
    "name": "AddDateIdExpiry5",
    "description": "Expiration date of additional document",
    "type": "Date"
  }, {
    "id": 953,
    "name": "AddDateIdIssuing5",
    "description": "Issuing date of additional document",
    "type": "Date"
  }, {
    "id": 952,
    "name": "AddIdNumber5",
    "description": "Id number of additional document",
    "type": "String"
  }, {
    "id": 951,
    "name": "AddIssuingCountry5",
    "description": "Issuing country of additional document",
    "type": "List"
  }, {"id": 950, "name": "AddDocType5", "description": "Type of additional document", "type": "List"}, {
    "id": 945,
    "name": "AddFile4",
    "description": "Scanned file of additional document",
    "type": "File"
  }, {
    "id": 944,
    "name": "AddDateIdExpiry4",
    "description": "Expiration date of additional document",
    "type": "Date"
  }, {
    "id": 943,
    "name": "AddDateIdIssuing4",
    "description": "Issuing date of additional document",
    "type": "Date"
  }, {
    "id": 942,
    "name": "AddIdNumber4",
    "description": "Id number of additional document",
    "type": "String"
  }, {
    "id": 941,
    "name": "AddIssuingCountry4",
    "description": "Issuing country of additional document",
    "type": "List"
  }, {"id": 940, "name": "AddDocType4", "description": "Type of additional document", "type": "List"}, {
    "id": 935,
    "name": "AddFile3",
    "description": "Scanned file of additional document",
    "type": "File"
  }, {
    "id": 934,
    "name": "AddDateIdExpiry3",
    "description": "Expiration date of additional document",
    "type": "Date"
  }, {
    "id": 933,
    "name": "AddDateIdIssuing3",
    "description": "Issuing date of additional document",
    "type": "Date"
  }, {
    "id": 932,
    "name": "AddIdNumber3",
    "description": "Id number of additional document",
    "type": "String"
  }, {
    "id": 931,
    "name": "AddIssuingCountry3",
    "description": "Issuing country of additional document",
    "type": "List"
  }, {"id": 930, "name": "AddDocType3", "description": "Type of additional document", "type": "List"}, {
    "id": 925,
    "name": "AddFile2",
    "description": "Scanned file of additional document",
    "type": "File"
  }, {
    "id": 924,
    "name": "AddDateIdExpiry2",
    "description": "Expiration date of additional document",
    "type": "Date"
  }, {
    "id": 923,
    "name": "AddDateIdIssuing2",
    "description": "Issuing date of additional document",
    "type": "Date"
  }, {
    "id": 922,
    "name": "AddIdNumber2",
    "description": "Id number of additional document",
    "type": "String"
  }, {
    "id": 921,
    "name": "AddIssuingCountry2",
    "description": "Issuing country of additional document",
    "type": "List"
  }, {"id": 920, "name": "AddDocType2", "description": "Type of additional document", "type": "List"}, {
    "id": 915,
    "name": "AddFile1",
    "description": "Scanned file of additional document",
    "type": "File"
  }, {
    "id": 914,
    "name": "AddDateIdExpiry1",
    "description": "Expiration date of additional document",
    "type": "Date"
  }, {
    "id": 913,
    "name": "AddDateIdIssuing1",
    "description": "Issuing date of additional document",
    "type": "Date"
  }, {
    "id": 912,
    "name": "AddIdNumber1",
    "description": "Id number of additional document",
    "type": "String"
  }, {
    "id": 911,
    "name": "AddIssuingCountry1",
    "description": "Issuing country of additional document",
    "type": "List"
  }, {"id": 910, "name": "AddDocType1", "description": "Type of additional document", "type": "List"}, {
    "id": 752,
    "name": "CardName5",
    "description": "Cardholder`s name",
    "type": "String"
  }, {"id": 751, "name": "CardExpiry5", "description": "Four-digit of card expiry date", "type": "String"}, {
    "id": 750,
    "name": "CardNumber5",
    "description": "Card number",
    "type": "String"
  }, {"id": 742, "name": "CardName4", "description": "Cardholder`s name", "type": "String"}, {
    "id": 741,
    "name": "CardExpiry4",
    "description": "Four-digit of card expiry date",
    "type": "String"
  }, {"id": 740, "name": "CardNumber4", "description": "Card number", "type": "String"}, {
    "id": 732,
    "name": "CardName3",
    "description": "Cardholder`s name",
    "type": "String"
  }, {"id": 731, "name": "CardExpiry3", "description": "Four-digit of card expiry date", "type": "String"}, {
    "id": 730,
    "name": "CardNumber3",
    "description": "Card number",
    "type": "String"
  }, {"id": 722, "name": "CardName2", "description": "Cardholder`s name", "type": "String"}, {
    "id": 721,
    "name": "CardExpiry2",
    "description": "Four-digit of card expiry date",
    "type": "String"
  }, {"id": 720, "name": "CardNumber2", "description": "Card number", "type": "String"}, {
    "id": 712,
    "name": "CardName1",
    "description": "Cardholder`s name",
    "type": "String"
  }, {"id": 711, "name": "CardExpiry1", "description": "Four-digit of card expiry date", "type": "String"}, {
    "id": 710,
    "name": "CardNumber1",
    "description": "Card number",
    "type": "String"
  }, {"id": 602, "name": "DocSignImage", "description": "Image of signature", "type": "File"}, {
    "id": 601,
    "name": "MRZDoc",
    "description": "MRZ document",
    "type": "String"
  }, {"id": 514, "name": "SanctionList", "description": "Sanction list", "type": "String"}, {
    "id": 513,
    "name": "PEPAssocDesc",
    "description": "Description of PEP`s family member / associate",
    "type": "String"
  }, {"id": 512, "name": "PEPAssociate", "description": "PEP`s family member / associate", "type": "Bool"}, {
    "id": 511,
    "name": "PEPDesc",
    "description": "PEP description",
    "type": "String"
  }, {"id": 510, "name": "PEP", "description": "Politically exposed person", "type": "Bool"}, {
    "id": 453,
    "name": "CvResume",
    "description": "Curriculum vitae or resume",
    "type": "String"
  }, {"id": 452, "name": "ReferenceLetter", "description": "Reference letter", "type": "String"}, {
    "id": 451,
    "name": "BankStatement",
    "description": "Bank statement / Bank reference letter",
    "type": "String"
  }, {"id": 450, "name": "SourceOfWealth", "description": "Source of wealth", "type": "List"}, {
    "id": 405,
    "name": "Phone5",
    "description": "Phone",
    "type": "Phone"
  }, {"id": 404, "name": "Phone4", "description": "Phone", "type": "Phone"}, {
    "id": 403,
    "name": "Phone3",
    "description": "Phone",
    "type": "Phone"
  }, {"id": 402, "name": "Phone2", "description": "Phone", "type": "Phone"}, {
    "id": 401,
    "name": "Phone1",
    "description": "Phone",
    "type": "Phone"
  }, {"id": 301, "name": "Selfie", "description": "Selfie", "type": "File"}, {
    "id": 209,
    "name": "ResAddressProofAdd",
    "description": "Residence proof document additional file",
    "type": "File"
  }, {
    "id": 208,
    "name": "ResAddressProofId",
    "description": "Residence proof document file",
    "type": "File"
  }, {
    "id": 207,
    "name": "ResOtherDoc",
    "description": "Type of residence proof document (for selected 'other' value)",
    "type": "String"
  }, {"id": 206, "name": "ResDocType", "description": "Type of residence proof document", "type": "List"}, {
    "id": 205,
    "name": "ResRegion",
    "description": "Region of residence",
    "type": "String"
  }, {"id": 204, "name": "ResPostalCode", "description": "Postal code of residence", "type": "String"}, {
    "id": 203,
    "name": "ResAddress",
    "description": "Address of residence",
    "type": "String"
  }, {"id": 202, "name": "ResCity", "description": "Сity/Town of residence", "type": "String"}, {
    "id": 201,
    "name": "ResCountry",
    "description": "Country of residence",
    "type": "List"
  }, {"id": 129, "name": "OccupationOther", "description": "Other occupation value", "type": "String"}, {
    "id": 128,
    "name": "Occupation",
    "description": "Information on the activity of the client",
    "type": "List"
  }, {"id": 127, "name": "Citizenship", "description": "Citizenship", "type": "String"}, {
    "id": 126,
    "name": "EMail",
    "description": "Email",
    "type": "Email"
  }, {"id": 124, "name": "Gender", "description": "Gender", "type": "String"}, {
    "id": 123,
    "name": "MiddleName",
    "description": "Middle name",
    "type": "String"
  }, {
    "id": 122,
    "name": "IntName",
    "description": "Name (international, transliterated)",
    "type": "String"
  }, {
    "id": 121,
    "name": "IntSurname",
    "description": "Surname (international, transliterated)",
    "type": "String"
  }, {
    "id": 114,
    "name": "WithMRZ",
    "description": "Identity document includes MRZ (Machine Readable Zone)",
    "type": "Bool"
  }, {"id": 113, "name": "IdType", "description": "Type of identity document", "type": "List"}, {
    "id": 112,
    "name": "BackOfIdCard",
    "description": "Back Side of Identity card",
    "type": "File"
  }, {"id": 111, "name": "IdFile", "description": "Identity document file", "type": "File"}, {
    "id": 109,
    "name": "DateOfIdExpiry",
    "description": "Identity document expiration date",
    "type": "Date"
  }, {
    "id": 108,
    "name": "DateOfIdIssuing",
    "description": "Identity document issuing date",
    "type": "Date"
  }, {"id": 107, "name": "IssuedBy", "description": "Identity document issued by", "type": "String"}, {
    "id": 106,
    "name": "IdNumber",
    "description": "Identity document number",
    "type": "String"
  }, {
    "id": 105,
    "name": "IssuingCountry",
    "description": "Identity document issuing country",
    "type": "List"
  }, {"id": 104, "name": "PlaceOfBirth", "description": "Country of birth", "type": "String"}, {
    "id": 103,
    "name": "DateOfBirth",
    "description": "Date of birth",
    "type": "Date"
  }, {
    "id": 102,
    "name": "Name",
    "description": "Local name (in the language of MultiPass)",
    "type": "String"
  }, {"id": 101, "name": "Surname", "description": "Local surname (in the language of MultiPass)", "type": "String"}],
};
