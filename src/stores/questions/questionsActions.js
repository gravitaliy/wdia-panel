import {mock} from "../../assets/mock/mock";

export class QuestionsActions {
  setQuestions(value) {
    this.questions = value;
  }

  handleReject(isReject, reject, initialData) {
    if (isReject) {
      this.setQuestions(initialData);
      reject();
    }
  }

  /* ========================= GET QUESTIONS LIST ============================= */

  handleGetQuestionsList() {
    setTimeout(() => {
      this.setQuestions(mock.questions);
    }, 1000);
/*
    const data = {
      promise: this.rootStore.api.getQuestionsList(),
      success: res => onSuccess(res),
      fullLoader: true,
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      this.setQuestions(data.data.rows);
    };
*/
  }

  /* ========================= POST QUESTION CHANGE STATE ============================= */

  handlePostQuestionChangeState(newData, initialData, resolve, reject) {
    setTimeout(() => {
      resolve();
      this.rootStore.actions.notification("Question state has been successfully changed", "success");
    }, 1000);
  }
/*
    const data = {
      promise: this.rootStore.api.postQuestionChangeState(newData),
      success: res => onSuccess(res),
      handleReject: isReject =>
        this.handleReject(isReject, reject, initialData),
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      resolve();
      this.rootStore.actions.notification(data.data.result, "success");
    };
  }
*/
}
