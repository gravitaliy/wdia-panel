import { decorate, observable } from "mobx";
import { QuestionsActions } from "./questionsActions";

export class QuestionsStore extends QuestionsActions {
  constructor(rootStore) {
    super();
    this.rootStore = rootStore;
  }

  questions = [];
}

decorate(QuestionsStore, {
  questions: observable,
});
