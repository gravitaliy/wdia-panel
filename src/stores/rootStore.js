import { Api } from "./api/api";
import { Actions } from "./actions";
import { SignInStore } from "./signIn/signInStore";
import { NotificationStore } from "./notification/notification";
import { UserDataStore } from "./userData/userDataStore";
import { CompaniesStore } from "./companies/companiesStore";
import { QuestionsStore } from "./questions/questionsStore";
import { ServicesStore } from "./services/servicesStore";
import { VerifiersStore } from "./verifiers/verifiersStore";
import { FieldsTemplatesStore } from "./fieldsTemplates/fieldsTemplatesStore";
import { UsersCompanyStore } from "./usersCompany/usersCompanyStore";
import { ServiceVerifiersStore } from "./serviceVerifiers/serviceVerifiersStore";
import { VerifierServicesStore } from "./verifierServices/verifierServicesStore";
import { RolesStore } from "./roles/rolesStore";
import { CommonResourcesStore } from "./commonResources/commonResourcesStore";

export class RootStore {
  constructor() {
    this.api = new Api(this);
    this.actions = new Actions(this);
    this.notificationStore = new NotificationStore();
    this.signInStore = new SignInStore(this);
    this.userDataStore = new UserDataStore(this);
    this.companiesListStore = new CompaniesStore(this);
    this.questionsStore = new QuestionsStore(this);
    this.servicesStore = new ServicesStore(this);
    this.verifiersStore = new VerifiersStore(this);
    this.fieldsTemplatesStore = new FieldsTemplatesStore(this);
    this.usersCompanyStore = new UsersCompanyStore(this);
    this.serviceVerifiersStore = new ServiceVerifiersStore(this);
    this.verifierServicesStore = new VerifierServicesStore(this);
    this.rolesStore = new RolesStore(this);
    this.commonResourcesStore = new CommonResourcesStore(this);
  }
}
