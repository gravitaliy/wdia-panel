import { observable, decorate } from "mobx";
import { RolesActions } from "./rolesActions";

export class RolesStore extends RolesActions {
  constructor(rootStore) {
    super();
    this.rootStore = rootStore;
  }

  roles = [];

  modules = {};
}

decorate(RolesStore, {
  roles: observable,
  modules: observable,
});
