import {mock} from "../../assets/mock/mock";

export class RolesActions {
  setRoles(value) {
    this.roles = value;
  }

  setModules(value) {
    this.modules = value;
  }

  handleReject(isReject, reject, initialData) {
    if (isReject) {
      this.setRoles(initialData);
      reject();
    }
  }

  /* ========================= GET MODULES ============================= */

  handleGetModules() {
    const data = {
      promise: this.rootStore.api.getModules(),
      success: res => onSuccess(res),
      fullLoader: true,
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      this.setModules(data.data.modules);
    };
  }

  /* ========================= GET ROLES LIST ============================= */

  handleGetRoles() {
    setTimeout(() => {
      this.setRoles(mock.roles);
    }, 300);
/*
    const data = {
      promise: this.rootStore.api.getRoles(),
      success: res => onSuccess(res),
      fullLoader: true,
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      this.setRoles(data.data.roles);
    };
*/
  }

  /* ========================= POST ROLE ADD ============================= */

  handlePostRoleAdd(newData, initialData, resolve, reject) {
    const data = {
      promise: this.rootStore.api.postRoleAdd(newData),
      success: () => onSuccess(),
      handleReject: isReject =>
        this.handleReject(isReject, reject, initialData),
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = () => {
      const data = {
        promise: this.rootStore.api.getRoles(),
        success: res => onGetFieldsSuccess(res),
      };

      this.rootStore.actions.handleSend(data);

      const onGetFieldsSuccess = ({ data }) => {
        this.setRoles(data.data.roles);
        resolve();
        this.rootStore.actions.notification(
          "Role has been successfully added",
          "success"
        );
      };
    };
  }

  /* ========================= POST ROLE UPDATE ============================= */

  handlePostRoleUpdate(newData, initialData, resolve, reject) {
    const data = {
      promise: this.rootStore.api.postRoleUpdate(newData),
      success: () => onSuccess(),
      handleReject: isReject =>
        this.handleReject(isReject, reject, initialData),
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = () => {
      resolve();
      this.rootStore.actions.notification(
        "Role has been successfully updated",
        "success"
      );
    };
  }


  /* ========================= POST ROLE REMOVE ============================= */

  handlePostRoleRemove(newData, initialData, resolve, reject) {
    const data = {
      promise: this.rootStore.api.postRoleRemove(newData),
      success: () => onSuccess(),
      handleReject: isReject =>
        this.handleReject(isReject, reject, initialData),
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = () => {
      resolve();
      this.rootStore.actions.notification(
        "Role has been successfully removed",
        "success"
      );
    };
  }

}
