import {mock} from "../../assets/mock/mock";

export class CommonResourcesActions {
  setCommonResources(value) {
    this.commonResources = value;
  }

  /* ========================= GET COMMON RESOURCES ============================= */

  handleGetCommonResources() {
    setTimeout(() => {
      this.setCommonResources(mock.commonResources);
    }, 1000);
/*
    const data = {
      promise: this.rootStore.api.getCommonResources(),
      success: (res) => onSuccess(res),
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      this.setCommonResources(data.data);
    };
*/
  }
}
