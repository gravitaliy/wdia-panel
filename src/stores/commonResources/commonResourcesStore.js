import { decorate, observable } from "mobx";
import { CommonResourcesActions } from "./commonResourcesActions";

export class CommonResourcesStore extends CommonResourcesActions {
  constructor(rootStore) {
    super();
    this.rootStore = rootStore;
  }

  commonResources = {};
}

decorate(CommonResourcesStore, {
  commonResources: observable,
});
