import { decorate, observable } from "mobx";
import { CompaniesActions } from "./companiesActions";

export class CompaniesStore extends CompaniesActions {
  constructor(rootStore) {
    super();
    this.rootStore = rootStore;
  }

  companies = [];
}

decorate(CompaniesStore, {
  companies: observable,
});
