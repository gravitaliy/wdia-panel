import {mock} from "../../assets/mock/mock";

export class CompaniesActions {
  setCompanies(value) {
    this.companies = value;
  }

  handleReject(isReject, reject, initialData) {
    if (isReject) {
      this.setCompanies(initialData);
      reject();
    }
  }

  /* ========================= GET COMPANIES LIST ============================= */

  handleGetCompaniesList() {
    setTimeout(() => {
      this.setCompanies(mock.companyList);
    }, 1000);
/*
    const data = {
      promise: this.rootStore.api.getCompanyList(),
      success: res => onSuccess(res),
      fullLoader: true,
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      this.setCompanies(data.data.rows);
    };
*/
  }

  /* ========================= POST COMPANY UPDATE ============================= */

  handlePostCompanyUpdate(newData, initialData, resolve, reject) {
    setTimeout(() => {
      resolve();
      this.rootStore.actions.notification(
        "Company has been successfully updated",
        "success"
      );
    }, 300);

    /*
        const data = {
          promise: this.rootStore.api.postCompanyUpdate(newData),
          success: () => onSuccess(),
          handleReject: isReject =>
            this.handleReject(isReject, reject, initialData),
        };

        this.rootStore.actions.handleSend(data);

        const onSuccess = () => {
          resolve();
          this.rootStore.actions.notification(
            "Company has been successfully updated",
            "success"
          );
        };
    */
  }
}
