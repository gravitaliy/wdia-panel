import { trackPromise } from "react-promise-tracker";

export class Actions {
  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  handleSend(data) {
    if (data.setIsPending) {
      data.setIsPending(true);
    }

    const promiseAction = () =>
      data.promise
        .then(res => {
          if (data.setIsPending) {
            data.setIsPending(false);
          }
          try {
            if (data.success) {
              data.success(res);
            }
          } catch (err) {
            if (data.handleReject) {
              data.handleReject(true);
            }
            this.notification(err);
          }
        })
        .catch(err => {
          if (data.setIsPending) {
            data.setIsPending(false);
          }
          if (data.handleReject) {
            data.handleReject(true);
          }
          if (err.response.data.data) {
            this.notification(err.response.data.data.message);
          } else {
            this.notification(err);
          }
        });

    if (data.fullLoader) {
      trackPromise(promiseAction());
    } else {
      promiseAction();
    }
  }

  notification(message, type, duration) {
    this.rootStore.notificationStore.setNotification({
      open: true,
      message,
      type,
      duration,
    });
  }
}
