import { routesList } from "../../assets/enums/routesList";
import { CookieHelper, COOKIES } from "../../assets/functions";

export class SignInActions {
  setSignIn(name, value) {
    this.signIn[name] = value;
  }

  handlePostAdminLogin(setIsPending) {
/*
    const data = {
      promise: this.rootStore.api.postAdminLogin({
        email: this.signIn.email,
        password: this.signIn.password,
      }),
      success: res => onSuccess(res),
      setIsPending,
    };
*/

    // this.rootStore.actions.handleSend(data);

    if (setIsPending) {
      setIsPending(true);
    }

    setTimeout(() => {
      CookieHelper.set(COOKIES.AUTHORIZATION, "token");
      if (setIsPending) {
        setIsPending(false);
      }
      window.location.href = routesList.SIGN_IN;
    }, 300);

    const onSuccess = () => {
      window.location.href = routesList.SIGN_IN;
    };
  }
}
