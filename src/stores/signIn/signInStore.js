import { decorate, observable } from "mobx";
import { SignInActions } from "./signInActions";

export class SignInStore extends SignInActions {
  constructor(rootStore) {
    super();
    this.rootStore = rootStore;
  }

  signIn = {
    email: "email@email.com",
    password: "111",
  };
}

decorate(SignInStore, {
  signIn: observable,
});
