import {mock} from "../../assets/mock/mock";

export class FieldsTemplatesActions {
  setFieldsTemplates(value) {
    this.fieldsTemplates = value;
  }

  setActiveFields(value) {
    this.activeFields = value;
  }

  setActiveName(value) {
    this.activeName = value;
  }

  setFieldList(value) {
    this.fieldList = value;
  }

  handleReject(isReject, reject, initialData) {
    if (isReject) {
      this.setFieldsTemplates(initialData);
      reject();
    }
  }

  handleRejectField(isReject, reject, initialData) {
    if (isReject) {
      this.setActiveFields(initialData);
      reject();
    }
  }

  /* ========================= GET FIELDS TEMPLATE LIST ============================= */

  handleGetFieldsTemplateList(id) {
    setTimeout(() => {
      this.setFieldsTemplates(mock.fieldsTemplates);

      if (id) {
        const findField = mock.fieldsTemplates.find(x => x.id === id);
        this.setActiveFields(findField.fields);
        this.setActiveName(findField.name);
      }
    }, 1000);
/*
    const data = {
      promise: this.rootStore.api.getFieldsTemplateList(),
      success: res => onSuccess(res),
      fullLoader: true,
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      this.setFieldsTemplates(data.data.fieldsTemplates);

      if (id) {
        const findField = data.data.fieldsTemplates.find(x => x.id === id);
        this.setActiveFields(findField.fields);
        this.setActiveName(findField.name);
      }
    };
*/
  }

  /* ========================= GET FIELDS LIST ============================= */

  handleGetFieldsList() {
    setTimeout(() => {
      this.setFieldList(mock.fields);
    }, 1000);
/*
    const data = {
      promise: this.rootStore.api.getFieldsList(),
      success: res => onSuccess(res),
      fullLoader: true,
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      this.setFieldList(data.data.fields);
    };
*/
  }

  /* ========================= POST FILDS TEMPLATE ADD ============================= */

  handlePostFildsTemplateAdd(newData, initialData, resolve, reject) {
    setTimeout(() => {
      // this.setFieldsTemplates(mock.fieldsTemplates);
      resolve();
      this.rootStore.actions.notification(
        "Field has been successfully added",
        "success"
      );
    }, 1000);
/*
    const data = {
      promise: this.rootStore.api.postFildsTemplateAdd(newData),
      success: () => onSuccess(),
      handleReject: isReject =>
        this.handleReject(isReject, reject, initialData),
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = () => {
      const data = {
        promise: this.rootStore.api.getFieldsTemplateList(),
        success: res => onGetFieldsSuccess(res),
      };

      this.rootStore.actions.handleSend(data);

      const onGetFieldsSuccess = ({ data }) => {
        this.setFieldsTemplates(data.data.fieldsTemplates);
        resolve();
        this.rootStore.actions.notification(
          "Field has been successfully added",
          "success"
        );
      };
    };
*/
  }

  /* ========================= POST FILDS TEMPLATE REMOVE ============================= */

  handlePostFildsTemplateRemove(newData, initialData, resolve, reject) {
    setTimeout(() => {
      resolve();
      this.rootStore.actions.notification(
        "Field has been successfully removed",
        "success"
      );
    }, 1000);
/*
    const data = {
      promise: this.rootStore.api.postFildsTemplateRemove(newData),
      success: () => onSuccess(),
      handleReject: isReject =>
        this.handleReject(isReject, reject, initialData),
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = () => {
      resolve();
      this.rootStore.actions.notification(
        "Field has been successfully removed",
        "success"
      );
    };
*/
  }

  /* ========================= POST FILDS TEMPLATE UPDATE ============================= */

  handlePostFildsTemplateUpdate(newData, initialData, resolve, reject) {
    setTimeout(() => {
      resolve();
      this.rootStore.actions.notification(
        "Field has been successfully updated",
        "success"
      );
    }, 1000);
  }
/*
    const data = {
      promise: this.rootStore.api.postFildsTemplateUpdate(newData),
      success: () => onSuccess(),
      handleReject: isReject =>
        this.handleRejectField(isReject, reject, initialData),
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = () => {
      resolve();
      this.rootStore.actions.notification(
        "Field has been successfully updated",
        "success"
      );
    };
  }
*/
}
