import { observable, decorate } from "mobx";
import { FieldsTemplatesActions } from "./fieldsTemplatesActions";

export class FieldsTemplatesStore extends FieldsTemplatesActions {
  constructor(rootStore) {
    super();
    this.rootStore = rootStore;
  }

  fieldsTemplates = [];

  activeFields = [];

  activeName = "";

  fieldList = [];
}

decorate(FieldsTemplatesStore, {
  fieldsTemplates: observable,
  activeFields: observable,
  activeName: observable,
  fieldList: observable,
});
