import { decorate, observable } from "mobx";

export class NotificationStore {
  notification = {
    open: false,
    message: "",
    type: null,
    duration: null,
  };

  setNotification({ open, message, type, duration }) {
    this.notification = {
      open,
      message,
      type: type || "error",
      duration: duration || 6000,
    };
  }
}

decorate(NotificationStore, {
  notification: observable,
});
