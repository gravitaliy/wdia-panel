import { decorate, observable } from "mobx";
import { ServicesActions } from "./servicesActions";

export class ServicesStore extends ServicesActions {
  constructor(rootStore) {
    super();
    this.rootStore = rootStore;
  }

  services = [];
}

decorate(ServicesStore, {
  services: observable,
});
