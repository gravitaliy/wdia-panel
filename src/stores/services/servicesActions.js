import {mock} from "../../assets/mock/mock";

export class ServicesActions {
  setServices(value) {
    this.services = value;
  }

  handleReject(isReject, reject, initialData) {
    if (isReject) {
      this.setServices(initialData);
      reject();
    }
  }

  /* ========================= GET SERVICE LIST ============================= */

  handleGetServiceList() {
    setTimeout(() => {
      this.setServices(mock.services);
    }, 1000);
/*
    const data = {
      promise: this.rootStore.api.getServiceList(),
      success: res => onSuccess(res),
      fullLoader: true,
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      this.setServices(data.data.rows);
    };
*/
  }

  /* ========================= POST UPDATE SERVICE ============================= */

  handlePostUpdateService(newData, initialData, resolve, reject) {
    setTimeout(() => {
      resolve();
      this.rootStore.actions.notification(
        "Service has been successfully updated",
        "success"
      );
    }, 500);
/*
    const data = {
      promise: this.rootStore.api.postUpdateService(newData),
      success: () => onSuccess(),
      handleReject: isReject =>
        this.handleReject(isReject, reject, initialData),
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = () => {
      resolve();
      this.rootStore.actions.notification(
        "Service has been successfully updated",
        "success"
      );
    };
*/
  }

  /* =================== POST ADD VERIFIERS LIST TO SERVICE =================== */

  handlePostAddVerifiersToService(newData) {
    setTimeout(() => {
      this.rootStore.actions.notification(
        "Verifiers has been successfully added",
        "success"
      );
    }, 1000);
  }
/*
    const data = {
      promise: this.rootStore.api.postAddVerifiersToServiceOrViceVersa(newData),
      success: () => onSuccess(),
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = () => {
      this.rootStore.actions.notification(
        "Verifiers has been successfully added",
        "success"
      );
    };
  }
*/
}
