import { decorate, observable } from "mobx";
import { VerifierServicesActions } from "./verifierServicesActions";

export class VerifierServicesStore extends VerifierServicesActions {
  constructor(rootStore) {
    super();
    this.rootStore = rootStore;
  }

  verifier = {};

  verifierServices = [];
}

decorate(VerifierServicesStore, {
  verifier: observable,
  verifierServices: observable,
});
