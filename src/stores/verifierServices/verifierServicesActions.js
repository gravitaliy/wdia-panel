import {mock} from "../../assets/mock/mock";

export class VerifierServicesActions {
  setVerifierServices(value) {
    this.verifierServices = value;
  }

  setVerifier(value) {
    this.verifier = value;
  }

  /* ========================= GET VERIFIER SERVICES ============================= */

  handleGetVerifierServices(hash) {
    setTimeout(() => {
      this.setVerifierServices(mock.verifierServices);
    }, 1000);
/*
    const data = {
      promise: this.rootStore.api.getVerifierServices({ hash }),
      success: res => onSuccess(res),
      fullLoader: true,
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      this.setVerifierServices(data.data.rows);
    };
*/
  }

  /* ========================= GET VERIFIER BY HASH ============================= */

  handleGetVerifierByHash(hash) {
    setTimeout(() => {
      const verifier = mock.verifiers.find(item => item.hash === hash);
      this.setVerifier(verifier);
    }, 1000);
/*
    const data = {
      promise: this.rootStore.api.getVerifierByHash({ hash }),
      success: res => onSuccess(res),
      fullLoader: true,
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      this.setVerifier(data.data.verifier);
    };
*/
  }
}
