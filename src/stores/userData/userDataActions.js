export class UserDataActions {
  setUser(name, value) {
    this.user[name] = value;
  }

  /* ========================= GET USER DATA ============================= */

  handleGetUserData() {
/*
    const data = {
      promise: this.rootStore.api.getUserData(),
      success: res => onSuccess(res),
    };
*/

    // this.rootStore.actions.handleSend(data);

    setTimeout(() => {
      this.setUser("email", "email@email.com");
    }, 300);

    const onSuccess = ({ data }) => {
      this.setUser("email", data.data.email);
    };
  }
}
