import { decorate, observable } from "mobx";
import { UserDataActions } from "./userDataActions";

export class UserDataStore extends UserDataActions {
  constructor(rootStore) {
    super();
    this.rootStore = rootStore;
  }

  user = {
    email: "",
  };
}

decorate(UserDataStore, {
  user: observable,
});
