import {mock} from "../../assets/mock/mock";

export class UsersCompanyActions {
  setCompanyUsers(value) {
    this.users = value;
  }

  setCompany(value) {
    this.company = value;
  }

  handleReject(isReject, reject, initialData) {
    if (isReject) {
      this.setCompanyUsers(initialData);
      reject();
    }
  }

  /* ========================= GET COMPANY USERS ============================= */

  handleGetCompanyUsers(id) {
    setTimeout(() => {
      this.setCompanyUsers(mock.companyUsers);
    }, 300);
/*
    const data = {
      promise: this.rootStore.api.getCompanyUsers({ id }),
      success: res => onSuccess(res),
      fullLoader: true,
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      this.setCompanyUsers(data.data.users);
    };
*/
  }

  /* ========================= GET COMPANY BY ID ============================= */

  handleGetCompanyById(id) {
    setTimeout(() => {
      const company = mock.companyList.find(item => item.id === Number(id));
      this.setCompany(company);
    }, 1000);
/*
    const data = {
      promise: this.rootStore.api.getCompanyById({ id }),
      success: res => onSuccess(res),
      fullLoader: true,
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      this.setCompany(data.data.company);
    };
*/
  }

  /* ========================= POST USER CHANGE ROLE ============================= */

  handlePostUserChangeRole(newData, initialData, resolve, reject) {
    setTimeout(() => {
      resolve();
      this.rootStore.actions.notification("Role has been successfully changed", "success");
    }, 300);
/*
    const data = {
      promise: this.rootStore.api.postUserChangeRole(newData),
      success: res => onSuccess(res),
      handleReject: isReject =>
        this.handleReject(isReject, reject, initialData),
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      resolve();
      this.rootStore.actions.notification(data.data, "success");
    };
*/
  }

  /* ========================= POST USER CHANGE STATE ============================= */

  handlePostUserChangeState(newData, initialData, resolve, reject) {
    setTimeout(() => {
      resolve();
      this.rootStore.actions.notification("State has been successfully changed", "success");
    }, 300);
/*
    const data = {
      promise: this.rootStore.api.postUserChangeState(newData),
      success: res => onSuccess(res),
      handleReject: isReject =>
        this.handleReject(isReject, reject, initialData),
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      resolve();
      this.rootStore.actions.notification(data.data, "success");
    };
*/
  }
}
