import { decorate, observable } from "mobx";
import { UsersCompanyActions } from "./usersCompanyActions";

export class UsersCompanyStore extends UsersCompanyActions {
  constructor(rootStore) {
    super();
    this.rootStore = rootStore;
  }

  company = {};

  users = [];
}

decorate(UsersCompanyStore, {
  company: observable,
  users: observable,
});
