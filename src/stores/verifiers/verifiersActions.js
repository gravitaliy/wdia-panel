import {mock} from "../../assets/mock/mock";

export class VerifiersActions {
  setVerifiers(value) {
    this.verifiers = value;
  }

  handleReject(isReject, reject, initialData) {
    if (isReject) {
      this.setVerifiers(initialData);
      reject();
    }
  }

  /* ========================= GET VERIFIERS LIST ============================= */

  handleGetVerifierList() {
    setTimeout(() => {
      this.setVerifiers(mock.verifiers);
    }, 1000);
/*
    const data = {
      promise: this.rootStore.api.getVerifierList(),
      success: res => onSuccess(res),
      fullLoader: true,
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      this.setVerifiers(data.data.rows);
    };
*/
  }

  /* ========================= POST UPDATE VERIFIER ============================= */

  handlePostUpdateVerifier(newData, initialData, resolve, reject) {
    setTimeout(() => {
      resolve();
      this.rootStore.actions.notification(
        "Verifier has been successfully updated",
        "success"
      );
    }, 1000);
/*
    const data = {
      promise: this.rootStore.api.postUpdateVerifier(newData),
      success: () => onSuccess(),
      handleReject: isReject =>
        this.handleReject(isReject, reject, initialData),
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = () => {
      resolve();
      this.rootStore.actions.notification(
        "Verifier has been successfully updated",
        "success"
      );
    };
*/
  }

  /* =================== POST ADD SERVICES LIST VERIFIER =================== */

  handlePostAddServicesToVerifier(newData) {
    setTimeout(() => {
      this.rootStore.actions.notification(
        "Services has been successfully added",
        "success"
      );
    }, 1000);
  }
/*
    const data = {
      promise: this.rootStore.api.postAddVerifiersToServiceOrViceVersa(newData),
      success: () => onSuccess(),
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = () => {
      this.rootStore.actions.notification(
        "Services has been successfully added",
        "success"
      );
    };
  }
*/
}
