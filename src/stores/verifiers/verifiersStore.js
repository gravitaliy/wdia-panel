import { decorate, observable } from "mobx";
import { VerifiersActions } from "./verifiersActions";

export class VerifiersStore extends VerifiersActions {
  constructor(rootStore) {
    super();
    this.rootStore = rootStore;
  }

  verifiers = [];
}

decorate(VerifiersStore, {
  verifiers: observable,
});
