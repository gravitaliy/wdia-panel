import axios from "axios";
import { CookieHelper, COOKIES } from "../../assets/functions";

const apiV1 = "/v1";

export class Api {
  constructor() {
    this.setAuth(CookieHelper.get(COOKIES.AUTHORIZATION));
  }

  /* ============================= AUTH ==================================== */

  setAuth(token) {
    if (!token) return;
    axios.defaults.headers.common["X-Token"] = token;
  }

  postAdminLogin(params) {
    return axios.post(`${apiV1}/admin/login`, params)
      .then(({ data }) => {
        CookieHelper.set(COOKIES.AUTHORIZATION, data.data.token);
        this.setAuth(data.data.token);
      });
  }

  /* ============================= COMMON RESOURCES ==================================== */

  getCommonResources(params) {
    return axios.get(`${apiV1}/common-resources`, { params });
  }

  /* ============================= USER ==================================== */

  getUserData(params) {
    return axios.get(`${apiV1}/user`, { params });
  }

  /* ============================= COMPANY ==================================== */

  getCompanyList(params) {
    return axios.get(`${apiV1}/company/list`, { params });
  }

  postCompanyUpdate(params) {
    return axios.post(`${apiV1}/admin/company/update`, params);
  }

  /* ============================= QUESTIONS ==================================== */

  getQuestionsList(params) {
    return axios.get(`${apiV1}/admin/question/list`, { params });
  }

  postQuestionChangeState(params) {
    return axios.post(`${apiV1}/admin/question/change-state`, params);
  }

  /* ============================= SERVICE ==================================== */

  getServiceList(params) {
    return axios.get(`${apiV1}/service/list`, { params });
  }

  postUpdateService(params) {
    return axios.post(`${apiV1}/admin/service/update`, params);
  }

  /* ============================= VERIFIER ==================================== */

  getVerifierList(params) {
    return axios.get(`${apiV1}/verifier/list`, { params });
  }

  postUpdateVerifier(params) {
    return axios.post(`${apiV1}/admin/verifier/update`, params);
  }

  /* ============================= FIELDS TEMPLATE ==================================== */

  getFieldsTemplateList(params) {
    return axios.get(`${apiV1}/fieldsTemplate/list`, { params });
  }

  getFieldsList(params) {
    return axios.get(`${apiV1}/field/list`, { params });
  }

  postFildsTemplateAdd(params) {
    return axios.post(`${apiV1}/admin/fildsTemplate/add`, params);
  }

  postFildsTemplateRemove(params) {
    return axios.post(`${apiV1}/admin/fildsTemplate/remove`, params);
  }

  postFildsTemplateUpdate(params) {
    return axios.post(`${apiV1}/admin/fildsTemplate/update`, params);
  }

  /* ============================= COMPANY USERS ==================================== */

  getCompanyUsers(params) {
    return axios.get(`${apiV1}/admin/company/users`, { params });
  }

  getCompanyById(params) {
    return axios.get(`${apiV1}/company`, { params });
  }

  postUserChangeRole(params) {
    return axios.post(`${apiV1}/admin/user/change-role`, params);
  }

  postUserChangeState(params) {
    return axios.post(`${apiV1}/admin/user/change-state`, params);
  }

  /* ============================= SERVICE VERIFIERS ==================================== */

  getServiceVerifiers(params) {
    return axios.get(`${apiV1}/service/verifiers`, { params });
  }

  getServiceByHash(params) {
    return axios.get(`${apiV1}/service`, { params });
  }

  /* ============================= VERIFIER SERVICES ==================================== */

  getVerifierServices(params) {
    return axios.get(`${apiV1}/verifier/services`, { params });
  }

  getVerifierByHash(params) {
    return axios.get(`${apiV1}/verifier`, { params });
  }

  /* ================= ADD VERIFIERS LIST TO SERVICE (AND VICE VERSA) ================= */

  postAddVerifiersToServiceOrViceVersa(params) {
    return axios.post(`${apiV1}/admin/service-verifiers`, params);
  }

  /* ============================= ROLES ==================================== */

  getModules(params) {
    return axios.get(`${apiV1}/admin/modules`, { params });
  }

  getRoles(params) {
    return axios.get(`${apiV1}/admin/roles`, { params });
  }

  postRoleAdd(params) {
    return axios.post(`${apiV1}/admin/role/add`, params);
  }

  postRoleUpdate(params) {
    return axios.post(`${apiV1}/admin/role/update`, params);
  }

  postRoleRemove(params) {
    return axios.post(`${apiV1}/admin/role/remove`, params);
  }
}
