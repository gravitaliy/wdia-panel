import { decorate, observable } from "mobx";
import { ServiceVerifiersActions } from "./serviceVerifiersActions";

export class ServiceVerifiersStore extends ServiceVerifiersActions {
  constructor(rootStore) {
    super();
    this.rootStore = rootStore;
  }

  service = {};

  serviceVerifiers = [];
}

decorate(ServiceVerifiersStore, {
  service: observable,
  serviceVerifiers: observable,
});
