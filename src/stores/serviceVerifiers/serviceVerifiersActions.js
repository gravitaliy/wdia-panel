import {mock} from "../../assets/mock/mock";

export class ServiceVerifiersActions {
  setServiceVerifiers(value) {
    this.serviceVerifiers = value;
  }

  setService(value) {
    this.service = value;
  }

  /* ========================= GET SERVICE VERIFIERS ============================= */

  handleGetServiceVerifiers(hash) {
    setTimeout(() => {
      this.setServiceVerifiers([]);
    }, 1000);
/*
    const data = {
      promise: this.rootStore.api.getServiceVerifiers({ hash }),
      success: res => onSuccess(res),
      fullLoader: true,
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      this.setServiceVerifiers(data.data.rows);
    };
*/
  }

  /* ========================= GET SERVICE BY HASH ============================= */

  handleGetServiceByHash(hash) {
    setTimeout(() => {
      const service = mock.services.find(item => item.hash === hash);
      this.setService(service);
    }, 1000);
/*
    const data = {
      promise: this.rootStore.api.getServiceByHash({ hash }),
      success: res => onSuccess(res),
      fullLoader: true,
    };

    this.rootStore.actions.handleSend(data);

    const onSuccess = ({ data }) => {
      this.setService(data.data.service);
    };
*/
  }
}
