import React from "react";
import { Route, Switch } from "react-router-dom";
import { observer } from "mobx-react";
import {
  Companies,
  FieldsTemplates,
  FieldTemplateFields,
  Questions,
  Roles,
  Services,
  ServiceVerifiers,
  UsersCompany,
  Verifiers,
  VerifierServices,
} from "../routes";
import { routesList } from "../assets/enums/routesList";

export const HomePage = observer(({ store }) => {
  return (
    <Switch>
      <Route
        exact
        path={routesList.COMPANIES}
        component={() => <Companies store={store} />}
        title="Companies"
      />
      <Route
        path={`${routesList.COMPANIES}/:id?`}
        component={() => <UsersCompany store={store} />}
        title="Company users"
      />
      <Route
        exact
        path={routesList.SERVICES}
        component={() => <Services store={store} />}
        title="Services"
      />
      <Route
        path={`${routesList.SERVICES}/:hash?`}
        component={() => <ServiceVerifiers store={store} />}
        title="Service verifiers"
      />
      <Route
        exact
        path={routesList.VERIFIERS}
        component={() => <Verifiers store={store} />}
        title="Verifiers"
      />
      <Route
        path={`${routesList.VERIFIERS}/:hash?`}
        component={() => <VerifierServices store={store} />}
        title="Verifier services"
      />
      <Route
        path={routesList.QUESTIONS}
        component={() => <Questions store={store} />}
        title="Questions"
      />
      <Route
        exact
        path={routesList.FIELDS_TEMPLATES}
        component={() => <FieldsTemplates store={store} />}
        title="Fields templates"
      />
      <Route
        path={`${routesList.FIELDS_TEMPLATES}/:id`}
        component={() => <FieldTemplateFields store={store} />}
        title="Field template fields"
      />
      <Route
        path={routesList.ROLES}
        component={() => <Roles store={store} />}
        title="Roles"
      />
    </Switch>
  );
});
