FROM node:lts

RUN mkdir -p /opt/dockerized_app

COPY . /opt/dockerized_app

WORKDIR /opt/dockerized_app

RUN yarn && yarn build
RUN chmod +x docker-entrypoint.sh

ENTRYPOINT [ "./docker-entrypoint.sh" ]