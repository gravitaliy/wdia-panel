# WDIA Panel

WDIA Panel web application

## Public urls

dev: [https://amazing-jones-8e9b70.netlify.app/](https://amazing-jones-8e9b70.netlify.app/)


## Installation

Use the package manager [yarn](https://yarnpkg.com/) to install multipass.

```bash
yarn
```

## Run app in dev mode

```bash
yarn watch
```

Runs the app in the development mode.
Open http://localhost:3003 to view it in the browser.

## Build

```bash
yarn build
```

## Start build

```bash
yarn start
```

## Fix code style

```bash
yarn format
```

## Start tests

```bash
yarn test
```

## Docker

1. Run in Docker locally:

```
HOST=0.0.0.0 PORT=7006 docker-compose up -d --build
```

2. Tests:

```
curl 0.0.0.0:7006
```
